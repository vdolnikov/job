<?php

use Laravel\Lumen\Testing\TestCase as BaseTestCase;
use Faker\Generator;
use Illuminate\Container\Container;


abstract class TestCase extends BaseTestCase
{
    /**
     * Инициализация Феркера для генерации данных
     *
     * @return Generator|mixed|object
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function faker()
    {
        return Container::getInstance()->make(Generator::class);
    }


    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__ . '/../bootstrap/app.php';
    }
}
