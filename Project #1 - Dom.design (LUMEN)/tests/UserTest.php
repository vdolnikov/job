<?php

use Illuminate\Support\Facades\DB;

/**
 * Class UserTest
 *
 * Тест для проверки работоспособности пользователя
 * Во время теста создется 2 пользователя и проверяется их работа, а так же методы авторизации
 */
class UserTest extends TestCase
{

    protected static $state;

    protected static $database;


    /**
     * Получить список специализаций для пользователей
     */
    public function testGetSpecializationsList()
    {
        $this->get("v1/specializations", []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            '*' => [
                "uuid",
                "name",
            ],
        ]);


        self::$state['specializations_list'] = $this->response;
    }

    /**
     * Получить специализацию по UUID
     */
    public function testGetSpecializationByUUID()
    {
        $this->get("v1/specializations/" . self::$state['specializations_list'][0]['uuid'], []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            "uuid",
            "name",
        ]);
    }

    /**
     * Создать новоых пользователей
     */
    public function testCreateNewUser()
    {
        $password = $this->faker()->password;


        $fixtures = [
            'positive_1' => [
                'data'   => [
                    'name'                  => $this->faker()->name,
                    'email'                 => 'unittest_' . $this->faker()->email,
                    'specialization_uuid'   => self::$state['specializations_list'][0]['uuid'],
                    'password_confirmation' => $password,
                    'password'              => $password,
                    'isBusiness'            => false,
                    'token'                 => config('custom.google_captcha_unittest_secret'),
                ],
                'result' => [
                    'save'          => true,
                    'statusCode'    => 201,
                    'jsonStructure' => [
                        "name",
                        "email",
                        "url_name",
                        "specialization_uuid",
                        "uuid",
                    ],
                ],
            ],
            'positive_2' => [
                'data'   => [
                    'name'                  => $this->faker()->name,
                    'email'                 => 'unittest_' . $this->faker()->email,
                    'specialization_uuid'   => self::$state['specializations_list'][1]['uuid'],
                    'password_confirmation' => $password,
                    'password'              => $password,
                    'isBusiness'            => true,
                    'token'                 => config('custom.google_captcha_unittest_secret'),
                ],
                'result' => [
                    'save'          => true,
                    'statusCode'    => 201,
                    'jsonStructure' => [
                        "name",
                        "email",
                        "url_name",
                        "specialization_uuid",
                        "uuid",
                    ],
                ],
            ],
            //Incorrect password
            'negative_1' => [
                'data'   => [
                    'name'                  => $this->faker()->name,
                    'email'                 => 'unittest_' . $this->faker()->email,
                    'specialization_uuid'   => self::$state['specializations_list'][1]['uuid'],
                    'password_confirmation' => '7654321',
                    'password'              => '1234567',
                    'isBusiness'            => true,
                    'token'                 => config('custom.google_captcha_unittest_secret'),
                ],
                'result' => [
                    'save'          => false,
                    'statusCode'    => 422,
                    'jsonStructure' => [
                        "password_confirmation",
                    ],
                ],
            ],
            //Incorrect recaptcha token
            'negative_2' => [
                'data'   => [
                    'name'                  => $this->faker()->name,
                    'email'                 => 'unittest_' . $this->faker()->email,
                    'specialization_uuid'   => self::$state['specializations_list'][1]['uuid'],
                    'password_confirmation' => $password,
                    'password'              => $password,
                    'isBusiness'            => true,
                    'token'                 => '123123123',
                ],
                'result' => [
                    'save'          => false,
                    'statusCode'    => 429,
                    'jsonStructure' => [
                        "message",
                    ],
                ],
            ],
        ];


        foreach ($fixtures as $fixture) {
            $this->post("v1/users", $fixture['data']);
            $this->seeStatusCode($fixture['result']['statusCode']);
            $this->seeJsonStructure($fixture['result']['jsonStructure']);

            // Сохраняем позитивную фикстуру
            if ($fixture['result']['save']) {
                $fixture['data']['uuid'] = $this->response['uuid'];
                self::$state['users'][]  = $fixture['data'];
            }
        }
    }

    /**
     * Получить список пользователй
     *
     * @return void
     */
    public function testGetUserList()
    {
        $this->get("v1/users", []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            "current_page",
            'data' => [
                '*' =>
                    [
                        'uuid',
                        'name',
                        'email',
                        'url_name',
                        'phone',
                        'website',
                        'avatar_url',
                        'email_verified_at',
                        'address',
                        'about',
                        'specialization_uuid',
                        "roles" => [
                            '*' =>
                                [
                                    "slug",
                                ],
                        ],
                    ],
            ],
            "first_page_url",
            "from",
            "next_page_url",
            "path",
            "per_page",
            "prev_page_url",
            "to",
        ]);
    }

    /**
     * Получить список пользователй
     *
     * @return void
     */
    public function testGetUserInfoByUUID()
    {
        $fixtures = [
            'positive_1' => [
                'data'   => [
                    'uuid' => self::$state['users'][0]['uuid'],
                ],
                'result' => [
                    'save'          => true,
                    'statusCode'    => 200,
                    'jsonStructure' => [
                        "uuid",
                        "name",
                        "email",
                        "url_name",
                        "phone",
                        "website",
                        "address",
                        'avatar_url',
                        'email_verified_at',
                        "about",
                        "specialization_uuid",
                        "specialization" => [
                            "uuid",
                            "name",
                        ],
                        "roles"          => [
                            '*' =>
                                [
                                    "slug",
                                ],
                        ],
                    ],
                ],
            ],
            'positive_2' => [
                'data'   => [
                    'uuid' => self::$state['users'][1]['uuid'],
                ],
                'result' => [
                    'save'          => false,
                    'statusCode'    => 200,
                    'jsonStructure' => [
                        "uuid",
                        "name",
                        "email",
                        "url_name",
                        "phone",
                        "website",
                        "address",
                        "about",
                        "specialization_uuid",
                        "specialization" => [
                            "uuid",
                            "name",
                        ],
                    ],
                ],
            ],
        ];

        foreach ($fixtures as $fixture) {
            $this->get("v1/users/" . $fixture['data']['uuid'], []);
            $this->seeStatusCode($fixture['result']['statusCode']);
            $this->seeJsonStructure($fixture['result']['jsonStructure']);
        }
    }

    /**
     * Авторизация пользователя
     */
    public function testLogin()
    {
        $fixtures = [
            'positive_1' => [
                'user_index' => 0,
                'data'       => [
                    'email'    => self::$state['users'][0]['email'],
                    'password' => self::$state['users'][0]['password'],
                    'token'    => config('custom.google_captcha_unittest_secret'),
                ],
                'result'     => [
                    'save'          => true,
                    'statusCode'    => 200,
                    'jsonStructure' => [
                        "token",
                        "token_type",
                    ],
                ],
            ],
            'positive_2' => [
                'user_index' => 1,
                'data'       => [
                    'email'    => self::$state['users'][1]['email'],
                    'password' => self::$state['users'][1]['password'],
                    'token'    => config('custom.google_captcha_unittest_secret'),
                ],
                'result'     => [
                    'save'          => true,
                    'statusCode'    => 200,
                    'jsonStructure' => [
                        "token",
                        "token_type",
                    ],
                ],
            ],
            // Вход из super admin
            'positive_3' => [
                'user_index' => 2,
                'data'       => [
                    'email'    => 'admin@dom.design',
                    'password' => '123456',
                    'token'    => config('custom.google_captcha_unittest_secret'),
                ],
                'result'     => [
                    'save'          => true,
                    'statusCode'    => 200,
                    'jsonStructure' => [
                        "token",
                        "token_type",
                    ],
                ],
            ],
            // Неправильный пароль
            'negative_1' => [
                'data'   => [
                    'email'    => self::$state['users'][1]['email'],
                    'password' => '123321',
                    'token'    => config('custom.google_captcha_unittest_secret'),
                ],
                'result' => [
                    'save'          => false,
                    'statusCode'    => 401,
                    'jsonStructure' => [
                        "message",
                    ],
                ],
            ],
            // google token неактуальный
            'negative_2' => [
                'data'   => [
                    'email'    => self::$state['users'][1]['email'],
                    'password' => '123321',
                    'token'    => 'sajdhasjhdjashdjah123',
                ],
                'result' => [
                    'save'          => false,
                    'statusCode'    => 429,
                    'jsonStructure' => [
                        "message",
                    ],
                ],
            ],
            // Токен не задан
            'negative_3' => [
                'data'   => [
                    'email'    => self::$state['users'][1]['email'],
                    'password' => '123321',
                ],
                'result' => [
                    'save'          => false,
                    'statusCode'    => 422,
                    'jsonStructure' => [
                        "token",
                    ],
                ],
            ],
        ];

        foreach ($fixtures as $fixture) {
            $this->json('post', 'v1/login', $fixture['data']);
            $this->seeStatusCode($fixture['result']['statusCode']);
            $this->seeJsonStructure($fixture['result']['jsonStructure']);

            if ($fixture['result']['save']) {
                self::$state['users'][$fixture['user_index']]['token'] = $this->response['token_type'] . ' ' . $this->response['token'];
            }
        }
    }

    /**
     * Подтверждение почты
     */
    public function testConfirmEmail()
    {
        $actionRequest1 = \App\Models\ActionRequest::where("params->email", self::$state['users'][0]['email'])->where('action', 'email.confirm')->first();
        $actionRequest2 = \App\Models\ActionRequest::where("params->email", self::$state['users'][1]['email'])->where('action', 'email.confirm')->first();

        $fixtures = [
            'positive_1' => [
                'hash'   => $actionRequest1->hash,
                'result' => [
                    'statusCode'    => 200,
                    'jsonStructure' => [
                        "success",
                    ],
                ],
            ],
            'positive_2' => [
                'hash'   => $actionRequest2->hash,
                'result' => [
                    'statusCode'    => 200,
                    'jsonStructure' => [
                        "success",
                    ],
                ],
            ],
            // Повтор
            'negative_1' => [
                'hash'   => $actionRequest1->hash,
                'result' => [
                    'statusCode'    => 404,
                    'jsonStructure' => [
                        "success",
                        "error",
                    ],
                ],
            ],
            // Несуществующий
            'negative_2' => [
                'hash'   => '123321',
                'result' => [
                    'statusCode'    => 404,
                    'jsonStructure' => [
                        "success",
                        "error",
                    ],
                ],
            ],
        ];

        foreach ($fixtures as $fixture) {
            $this->json('post', 'v1/action/email/confirm/' . $fixture['hash'], []);
            $this->seeStatusCode($fixture['result']['statusCode']);
            $this->seeJsonStructure($fixture['result']['jsonStructure']);

        }
    }

    /**
     * Создать новоых пользователей
     */
    public function testUpdateUserByUUID()
    {

        $successStructure = [
            "uuid",
            "name",
            "email",
            "url_name",
            "phone",
            "website",
            "address",
            "about",
            "specialization_uuid",
        ];


        $fixtures = [
            'positive_1' => [
                'user_index' => 0,
                'header'     => [
                    'Authorization' => self::$state['users'][0]['token'],
                ],
                'user_uuid'  => self::$state['users'][0]['uuid'],
                'data'       => [
                    "name"                => $this->faker()->name,
                    "url_name"            => (string)$this->faker()->numberBetween($min = 1000000000, $max = 2147483647),
                    "phone"               => 'www.site' . $this->faker()->numberBetween($min = 1000, $max = 9000) . '.ru',
                    "website"             => '+7983138' . $this->faker()->numberBetween($min = 10000, $max = 90000),
                    "address"             => $this->faker()->address,
                    "about"               => $this->faker()->sentence(20),
                    "specialization_uuid" => self::$state['specializations_list'][3]['uuid'],
                ],
                'result'     => [
                    'save'          => true,
                    'statusCode'    => 200,
                    'jsonStructure' => $successStructure,
                ],
            ],
            'positive_2' => [
                'user_index' => 1,
                'header'     => [
                    'Authorization' => self::$state['users'][1]['token'],
                ],
                'user_uuid'  => self::$state['users'][1]['uuid'],
                'data'       => [
                    "name"                => $this->faker()->name,
                    "url_name"            => (string)$this->faker()->numberBetween($min = 1000000000, $max = 2147483647),
                    "phone"               => 'www.site' . $this->faker()->numberBetween($min = 1000, $max = 9000) . '.ru',
                    "website"             => '+7983138' . $this->faker()->numberBetween($min = 10000, $max = 90000),
                    "address"             => $this->faker()->address,
                    "about"               => $this->faker()->sentence(20),
                    "specialization_uuid" => self::$state['specializations_list'][4]['uuid'],
                ],
                'result'     => [
                    'save'          => true,
                    'statusCode'    => 200,
                    'jsonStructure' => $successStructure,
                ],
            ],
            // Не авторизован
            'negative_1' => [
                'user_index' => 0,
                'header'     => [
                    'Authorization' => '123',
                ],
                'user_uuid'  => self::$state['users'][0]['uuid'],
                'data'       => [
                    "name"                => $this->faker()->name,
                    "url_name"            => (string)$this->faker()->numberBetween($min = 1000000000, $max = 2147483647),
                    "phone"               => 'www.site' . $this->faker()->numberBetween($min = 1000, $max = 9000) . '.ru',
                    "website"             => '+7983138' . $this->faker()->numberBetween($min = 10000, $max = 90000),
                    "address"             => $this->faker()->address,
                    "about"               => $this->faker()->sentence(20),
                    "specialization_uuid" => self::$state['specializations_list'][3]['uuid'],
                ],
                'result'     => [
                    'save'          => false,
                    'statusCode'    => 401,
                    'jsonStructure' => ['error'],
                ],
            ],
            // Авторизован, но под другим пользователем
            'negative_2' => [
                'user_index' => 0,
                'header'     => [
                    'Authorization' => self::$state['users'][1]['token'],
                ],
                'user_uuid'  => self::$state['users'][0]['uuid'],
                'data'       => [
                    "name"                => $this->faker()->name,
                    "url_name"            => (string)$this->faker()->numberBetween($min = 1000000000, $max = 2147483647),
                    "phone"               => 'www.site' . $this->faker()->numberBetween($min = 1000, $max = 9000) . '.ru',
                    "website"             => '+7983138' . $this->faker()->numberBetween($min = 10000, $max = 90000),
                    "address"             => $this->faker()->address,
                    "about"               => $this->faker()->sentence(20),
                    "specialization_uuid" => self::$state['specializations_list'][3]['uuid'],
                ],
                'result'     => [
                    'save'          => false,
                    'statusCode'    => 403,
                    'jsonStructure' => ['error'],
                ],
            ],
        ];


        foreach ($fixtures as $fixture) {
            $result = $this->patch('v1/users/' . $fixture['user_uuid'], $fixture['data'], $fixture['header']);
            $this->seeStatusCode($fixture['result']['statusCode']);
            $this->seeJsonStructure($fixture['result']['jsonStructure']);
            $this->refreshApplication();

            $responseAttrs = json_decode($result->response->content());

            // Проверяем что данные действительно изменились
            foreach ($responseAttrs as $responseAttrKey => $responseAttrVal){
                if(isset($fixture['data'][$responseAttrKey])){
                    $this->assertEquals($responseAttrVal, $fixture['data'][$responseAttrKey]);
                }
            }

            if ($fixture['result']['save']) {
                foreach ($successStructure as $element) {
                    self::$state['users'][$fixture['user_index']][$element] = $this->response[$element];
                }
            }
        }
    }

    /**
     * Смена пароля пользователя
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function testChangePasswordByUUID()
    {
        $newPassword = $this->faker()->password;

        $fixtures = [
            'positive_1' => [
                'user_index' => 0,
                'header'     => [
                    'Authorization' => self::$state['users'][0]['token'],
                ],
                'user_uuid'  => self::$state['users'][0]['uuid'],
                'data'       => [
                    "password"              => $newPassword,
                    "password_confirmation" => $newPassword,
                    "password_old"          => self::$state['users'][0]['password'],
                ],
                'result'     => [
                    'save'          => true,
                    'statusCode'    => 200,
                    'jsonStructure' => ['success'],
                ],
            ],
            // Умена из под супер админа
            'positive_2' => [
                'user_index' => 1,
                'header'     => [
                    'Authorization' => self::$state['users'][2]['token'],
                ],
                'user_uuid'  => self::$state['users'][1]['uuid'],
                'data'       => [
                    "password"              => $newPassword,
                    "password_confirmation" => $newPassword,
                    "password_old"          => self::$state['users'][1]['password'],
                ],
                'result'     => [
                    'save'          => true,
                    'statusCode'    => 200,
                    'jsonStructure' => ['success'],
                ],
            ],
            // Неавторизован
            'negative_1' => [
                'user_index' => 0,
                'header'     => [],
                'user_uuid'  => self::$state['users'][0]['uuid'],
                'data'       => [
                    "password"              => $newPassword,
                    "password_confirmation" => $newPassword,
                    "password_old"          => self::$state['users'][0]['password'],
                ],
                'result'     => [
                    'save'          => false,
                    'statusCode'    => 401,
                    'jsonStructure' => ['error'],
                ],
            ],
            // Под чужим аккаунтом
            'negative_2' => [
                'user_index' => 0,
                'header'     => [
                    'Authorization' => self::$state['users'][1]['token'],
                ],
                'user_uuid'  => self::$state['users'][0]['uuid'],
                'data'       => [
                    "password"              => $newPassword,
                    "password_confirmation" => $newPassword,
                    "password_old"          => self::$state['users'][0]['password'],
                ],
                'result'     => [
                    'save'          => false,
                    'statusCode'    => 403,
                    'jsonStructure' => ['success', 'error'],
                ],
            ],
            // Неправильный страый пароль
            'negative_3' => [
                'user_index' => 0,
                'header'     => [
                    'Authorization' => self::$state['users'][0]['token'],
                ],
                'user_uuid'  => self::$state['users'][0]['uuid'],
                'data'       => [
                    "password"              => $newPassword,
                    "password_confirmation" => $newPassword,
                    "password_old"          => "123321",
                ],
                'result'     => [
                    'save'          => false,
                    'statusCode'    => 422,
                    'jsonStructure' => ['password_old'],
                ],
            ],
        ];


        foreach ($fixtures as $fixture) {
            $this->patch('v1/users/' . $fixture['user_uuid'] . '/password/change', $fixture['data'], $fixture['header']);
            $this->seeStatusCode($fixture['result']['statusCode']);
            $this->seeJsonStructure($fixture['result']['jsonStructure']);
            $this->refreshApplication();

            if ($fixture['result']['save']) {
                self::$state['users'][$fixture['user_index']]['password'] = $fixture['data']['password'];
            }
        }

    }

    /**
     * Обновить JWT токен
     */
    public function testRefreshToken()
    {
        $fixtures = [
            'positive_1' => [
                'user_index' => 0,
                'header'     => ['Authorization' => self::$state['users'][0]['token']],
                'result'     => [
                    'save'          => true,
                    'statusCode'    => 200,
                    'jsonStructure' => ['token', 'token_type', 'expires_in'],
                ],
            ],
        ];

        foreach ($fixtures as $fixture) {
            $this->post('v1/refresh', [], $fixture['header']);
            $this->seeStatusCode($fixture['result']['statusCode']);
            $this->seeJsonStructure($fixture['result']['jsonStructure']);
            $this->refreshApplication();

            if ($fixture['result']['save']) {
                self::$state['users'][$fixture['user_index']]['token'] = $this->response['token_type'] . ' ' . $this->response['token'];
            }
        }
    }

    /**
     * Получить профиль пользователя по токену
     */
    public function testGetProfile()
    {
        $jsonStructure = [
            "user" => [
                "uuid",
                "name",
                "email",
                "url_name",
                "phone",
                "website",
                "address",
                'avatar_url',
                'email_verified_at',
                "about",
                "specialization_uuid",
                "roles" => [
                    '*' =>
                        [
                            "slug",
                        ],
                ],
            ],
        ];

        $fixtures = [
            'positive_1' => [
                'user_index' => 0,
                'header'     => ['Authorization' => self::$state['users'][0]['token']],
                'result'     => [
                    'save'          => false,
                    'statusCode'    => 200,
                    'jsonStructure' => $jsonStructure,
                ],
            ],
            'positive_2' => [
                'user_index' => 1,
                'header'     => ['Authorization' => self::$state['users'][1]['token']],
                'result'     => [
                    'save'          => false,
                    'statusCode'    => 200,
                    'jsonStructure' => $jsonStructure,
                ],
            ],
        ];

        foreach ($fixtures as $fixture) {
            $this->get('v1/profile', $fixture['header']);
            $this->seeStatusCode($fixture['result']['statusCode']);
            $this->seeJsonStructure($fixture['result']['jsonStructure']);
            $this->refreshApplication();
        }
    }

    /**
     * Выходим из профиля и еще раз пытаемся получить информацию о профиле
     * Должна вернуться ошибка
     *
     */
    public function testLogout()
    {

        $fixtures = [
            'positive_1' => [
                'user_index' => 0,
                'header'     => ['Authorization' => self::$state['users'][0]['token']],
                'result'     => [
                    'save'                   => false,
                    'statusCode'             => 200,
                    'statusCodeSecondMethod' => 401,
                    'jsonStructure'          => ['success'],
                ],
            ],
//            'positive_2' => [
//                'user_index' => 1,
//                'header'     => ['Authorization' => self::$state['users'][1]['token']],
//                'result'     => [
//                    'save'                   => false,
//                    'statusCode'             => 200,
//                    'statusCodeSecondMethod' => 401,
//                    'jsonStructure'          => ['success'],
//                ],
//            ],
        ];

        foreach ($fixtures as $fixture) {

            $this->get('v1/logout', $fixture['header']);
            $this->seeStatusCode($fixture['result']['statusCode']);
            $this->seeJsonStructure($fixture['result']['jsonStructure']);

            $this->get('v1/profile', $fixture['header']);
            $this->seeStatusCode($fixture['result']['statusCodeSecondMethod']);

            $this->refreshApplication();
        }
    }

    /**
     * Отправка письма для смены пароля
     */
    public function testResetPasswordSendEmail()
    {
        $fixtures = [
            'positive_1' => [
                'data'   => [
                    "email" => self::$state['users'][0]['email'],
                    'token' => config('custom.google_captcha_unittest_secret'),
                ],
                'result' => [
                    'statusCode'    => 200,
                    'jsonStructure' => ['success'],
                ],
            ],
            // Несуществующий пользователь
            'negative_1' => [
                'data'   => [
                    "email" => 'example@mail.ru',
                    'token' => config('custom.google_captcha_unittest_secret'),
                ],
                'result' => [
                    'statusCode'    => 404,
                    'jsonStructure' => ['success', 'error'],
                ],
            ],
            // Несуществующий токен для рекапчи
            'negative_2' => [
                'data'   => [
                    "email" => self::$state['users'][0]['email'],
                    'token' => "asdasdknajsdnjasd",
                ],
                'result' => [
                    'statusCode'    => 429,
                    'jsonStructure' => ['message'],
                ],
            ],
        ];

        foreach ($fixtures as $fixture) {
            $this->json('post', 'v1/email/password/reset', $fixture['data']);
            $this->seeStatusCode($fixture['result']['statusCode']);
            $this->seeJsonStructure($fixture['result']['jsonStructure']);
        }
    }


    /**
     * Отправка письма для смены пароля
     */
    public function testResetPassword()
    {
        $actionRequest = \App\Models\ActionRequest::where("params->email", self::$state['users'][0]['email'])->where('action', 'password.reset')->first();

        $fixtures = [
            // Несуществующий хешь
            'negative_1' => [
                'hash'   => '123321',
                'data'   => [
                    "password"              => "newPassword",
                    "password_confirmation" => "newPassword",
                ],
                'result' => [
                    'save'          => false,
                    'statusCode'    => 404,
                    'jsonStructure' => [
                        "success",
                        "error",
                    ],
                ],
            ],
            // Разные пароли
            'negative_2' => [
                'hash'   => '123321',
                'data'   => [
                    "password"              => "newPassword",
                    "password_confirmation" => "oldPassword",
                ],
                'result' => [
                    'save'          => false,
                    'statusCode'    => 422,
                    'jsonStructure' => [
                        "password_confirmation",
                    ],
                ],
            ],
            // Короткий пароли, меньше 5 знаков
            'negative_3' => [
                'hash'   => '123321',
                'data'   => [
                    "password"              => "12",
                    "password_confirmation" => "12",
                ],
                'result' => [
                    'save'          => false,
                    'statusCode'    => 422,
                    'jsonStructure' => [
                        "password",
                    ],
                ],
            ],
            'positive_1' => [
                'hash'       => $actionRequest->hash,
                'user_index' => 0,
                'data'       => [
                    "password"              => "newPassword",
                    "password_confirmation" => "newPassword",
                ],
                'result'     => [
                    'save'          => true,
                    'statusCode'    => 200,
                    'jsonStructure' => [
                        "success",
                    ],
                ],
            ],
            // Повторное изспользование хеша
            'negative_4' => [
                'hash'   => $actionRequest->hash,
                'data'   => [
                    "password"              => "newPassword",
                    "password_confirmation" => "newPassword",
                ],
                'result' => [
                    'save'          => false,
                    'statusCode'    => 404,
                    'jsonStructure' => [
                        "success",
                        "error",
                    ],
                ],
            ],
        ];

        foreach ($fixtures as $fixture) {
            $this->json('post', 'v1/action/password/reset/' . $fixture['hash'], $fixture['data']);
            $this->seeStatusCode($fixture['result']['statusCode']);
            $this->seeJsonStructure($fixture['result']['jsonStructure']);

            if ($fixture['result']['save']) {
                self::$state['users'][$fixture['user_index']]['password'] = $fixture['data']['password'];
            }
        }

    }

    /**
     * Повторный логин с новым паролем
     */
    public function testLoginWithNewPassword()
    {
        $this->testLogin();
        $this->testLogout();
    }


    /**
     * Удаляем пользователя
     */
    public function testDeleteUser()
    {
        $fixtures = [
            //Удаление своего аккаунта
            'negative_1' => [
                'user_index' => 0,
                'header'     => ['Authorization' => self::$state['users'][0]['token']],
                'user_uuid'  => self::$state['users'][0]['uuid'],
                'result'     => [
                    'save'          => false,
                    'statusCode'    => 401,
                    'jsonStructure' => ['error'],
                ],
            ],
            //Удаление без авторизации
            'negative_2' => [
                'user_index' => 0,
                'user_uuid'  => self::$state['users'][0]['uuid'],
                'header'     => [],
                'result'     => [
                    'save'          => false,
                    'statusCode'    => 401,
                    'jsonStructure' => ['error'],
                ],
            ],
            //Удаление чужого аккаунта
            'negative_3' => [
                'user_index' => 0,
                'header'     => ['Authorization' => self::$state['users'][0]['token']],
                'user_uuid'  => self::$state['users'][1]['uuid'],
                'result'     => [
                    'save'          => false,
                    'statusCode'    => 401,
                    'jsonStructure' => ['error'],
                ],
            ],
            // Удаление из под супер админа
            'positive_1' => [
                'user_index' => 2,
                'header'     => ['Authorization' => self::$state['users'][2]['token']],
                'user_uuid'  => self::$state['users'][0]['uuid'],
                'result'     => [
                    'save'          => false,
                    'statusCode'    => 200,
                    'jsonStructure' => ['success'],
                ],
            ],
            'positive_2' => [
                'user_index' => 1,
                'header'     => ['Authorization' => self::$state['users'][2]['token']],
                'user_uuid'  => self::$state['users'][1]['uuid'],
                'result'     => [
                    'save'          => false,
                    'statusCode'    => 200,
                    'jsonStructure' => ['success'],
                ],
            ],
        ];

        foreach ($fixtures as $fixture) {
            $this->delete('v1/users/' . $fixture['user_uuid'], [], $fixture['header']);
            $this->seeStatusCode($fixture['result']['statusCode']);
            $this->seeJsonStructure($fixture['result']['jsonStructure']);

            $this->refreshApplication();
        }
    }

    /////////////////////////////////////////
    ///
    /// УДАЛЯЕМ ИСПОЛЬЗУЕМЫХ ПОЛЬЗОВАТЕЛЕЙ
    ///
    /////////////////////////////////////////
    public function testDeleteUsedUsers()
    {
        foreach (self::$state['users'] as $user) {
            if (isset($user['uuid'])) {
                DB::table('users')->where('uuid', '=', $user['uuid'])->delete();
            }
        }

        $this->assertTrue(true);
    }


}
