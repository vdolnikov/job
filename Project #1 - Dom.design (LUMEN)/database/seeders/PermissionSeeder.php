<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $manageUser = new Permission();
        $manageUser->name = 'Add project';
        $manageUser->slug = 'add-project';
        $manageUser->save();

        $createTasks = new Permission();
        $createTasks->name = 'Edit designers profile';
        $createTasks->slug = 'edit-designers-profile';
        $createTasks->save();
    }
}
