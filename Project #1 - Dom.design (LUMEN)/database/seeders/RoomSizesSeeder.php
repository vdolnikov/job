<?php

namespace Database\Seeders;

use App\Models\RoomSize;
use Illuminate\Database\Seeder;

class RoomSizesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roomSizes = [
            'Компактный',
            'Средний',
            'Большой',
            'Огромный',
        ];


        foreach ($roomSizes as $roomSize) {
            $roomSizesClass = new RoomSize();
            $roomSizesClass->name = $roomSize;
            $roomSizesClass->save();
        }

    }
}


