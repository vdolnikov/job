<?php

namespace Database\Seeders;

use App\Helper\ColorDiff;
use App\Models\Color;
use App\Models\ColorGroup;
use Illuminate\Database\Seeder;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $colors = [
            '425E17',
            '9F8200',
            '004524',
            'EF3038',
            'A9203E',
            'FF4040',
            '4D220E',
            '490005',
            'A91D11',
            '641349',
            '7B001C',
            '142300',
            'D76E00',
            'C34D0A',
            '6F0035',
            'EB5284',
            '1A153F',
            '531A50',
            'FF1493',
            '00382B',
            '002F55',
            '240935',
            '423189',
            '606E8C',
            '42AAFF',
            '00BFFF',
            'A2A2D0',
            '80DAEB',
            '0E294B',
            '30BA8F',
            '87CEEB',
            'FFDB58',
            'FD7C6E',
            'F34723',
            '2F353B',
        ];

        $colorGroup = new ColorGroup();
        $list = $colorGroup->getColorGroupList();
        foreach ($colors as $color) {
            $colorClass                   = new Color();
            $colorClass->color_group_uuid = ColorDiff::getCloseColorGroupByHex($color, $list)->uuid;
            $colorClass->hex_code = $color;

            // Если ругается что uuid не найден, значит нужно почистить кеш
            $colorClass->save();
        }

    }
}


