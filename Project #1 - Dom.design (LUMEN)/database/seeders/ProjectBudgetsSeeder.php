<?php

namespace Database\Seeders;

use App\Models\ProjectBudget;
use Illuminate\Database\Seeder;

class ProjectBudgetsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $projectBudgets = [
            'Низкий',
            'Средний',
            'Высокий',
            'Люкс',
        ];

        foreach ($projectBudgets as $projectBudget) {
            $s = new ProjectBudget();
            $s->name = $projectBudget;
            $s->save();
        }
    }
}
