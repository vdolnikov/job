<?php

namespace Database\Seeders;

use App\Models\ColorGroup;
use Illuminate\Database\Seeder;

class ColorGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $colorGroups = [
            'Красный'       => 'ff0000',
            'Оранжевый'     => 'ffa500',
            'Розовый'       => 'ffc0cb',
            'Фиолетовый'    => '8b00ff',
            'Синий'         => '0000ff',
            'Бирюзовый'     => '40e0d0',
            'Голубой'       => '42aaff',
            'Сиреневый'     => 'c8a2c8',
            'Зеленый'       => '008000',
            'Темно-зеленый' => '013220',
            'Золотой'       => 'ffd700',
            'Желтый'        => 'ffff00',
            'Бежевый'       => 'c8ad7f',
            'Коричневый'    => '964b00',
            'Серый'         => '999999',
            'Белый'         => 'ffffff',
            'Черный'        => '000000',
            'Салатовый'     => '99ff99',
        ];


        foreach ($colorGroups as $color_name => $color_hex) {
            $ColorGroupClass           = new ColorGroup();
            $ColorGroupClass->name     = $color_name;
            $ColorGroupClass->hex_code = $color_hex;
            $ColorGroupClass->save();
        }

    }
}


