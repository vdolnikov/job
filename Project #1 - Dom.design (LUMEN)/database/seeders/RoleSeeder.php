<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roule       = new Role();
        $roule->name = 'Super admin';
        $roule->slug = 'super-admin';
        $roule->save();

        $roule       = new Role();
        $roule->name = 'Admin';
        $roule->slug = 'admin';
        $roule->save();

        $roule       = new Role();
        $roule->name = 'Business';
        $roule->slug = 'business';
        $roule->save();

        $roule       = new Role();
        $roule->name = 'User';
        $roule->slug = 'user';
        $roule->save();
    }
}
