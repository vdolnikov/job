<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SpecializationSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(ProjectStylesSeeder::class);
        $this->call(ProjectBudgetsSeeder::class);
        $this->call(RoomsSeeder::class);
        $this->call(ColorGroupSeeder::class);
        $this->call(ColorSeeder::class);
        $this->call(RoomSizesSeeder::class);
    }
}
