<?php

namespace Database\Seeders;

use App\Models\ProjectStyle;
use Illuminate\Database\Seeder;

class ProjectStylesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $projectStyles = [
            'Эклектика',
            'Ретро',
            'Классический',
            'Лофт',
            'Скандинавский',
            'Современный',
            'Хай-тек',
            'Восточный',
            'Эко',
            'Минимализм',
            'Прованс и кантри',
            'Викторианский',
            'Фьюжн',
            'Модернизм',
        ];

        foreach ($projectStyles as $projectStyle) {
            $s = new ProjectStyle();
            $s->name = $projectStyle;
            $s->save();
        }
    }
}
