<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Specialization;
use Illuminate\Database\Seeder;

class SpecializationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $specializations = [
            'Дизайн-студия',
            'Частный дизайнер',
            'Декоратор',
            'Строительство и ремонт',
            'Строитель',
            'Фотограф',
            'Мастер по ремонту',
            'Архитектурное бюро',
            'Другое',
        ];

        foreach ($specializations as $specialization) {
            $s = new Specialization();
            $s->name = $specialization;
            $s->save();
        }

    }
}
