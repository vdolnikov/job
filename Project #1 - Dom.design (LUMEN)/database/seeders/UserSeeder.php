<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $super_admin_rule = Role::where('slug', 'super-admin')->first();
        $business_rule    = Role::where('slug', 'business')->first();

        $permission1 = Permission::where('slug', 'add-project')->first();
        $permission2 = Permission::where('slug', 'edit-business-profile')->first();

        // Добавление пермишенов для роли
        $business_rule->permissions()->attach($permission1);
        $business_rule->permissions()->attach($permission2);


        $user1           = new User();
        $user1->name     = 'Dom.design admin';
        $user1->email    = 'admin@dom.design';
        $user1->password = '123456';
        $user1->url_name = 'dom_design_admin';
        $user1->save();
        // Добавление роли пользователю
        $user1->roles()->attach($super_admin_rule);


        $user2           = new User();
        $user2->name     = 'Dom.design';
        $user2->email    = 'designer@dom.design';
        $user2->password = '123456';
        $user2->url_name = 'dom_design';
        $user2->save();
        // Добавление роли пользователю
        $user2->roles()->attach($business_rule);

        // Добавление пермишенов для пользователя
        // $user2->permissions()->attach($permission1);
        // $user2->permissions()->attach($permission2);
    }
}
