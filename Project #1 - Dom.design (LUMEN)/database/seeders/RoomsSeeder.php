<?php

namespace Database\Seeders;

use App\Models\Room;
use Illuminate\Database\Seeder;

class RoomsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rooms = [
            ['Кухня', 'kuhnya', 'Кухни', 1],
            ['Ванная', 'vannaya', 'Ванной', 2],
            ['Прихожая (Коридор)', 'prihozhaya', 'Прихожей (Коридора)', 5],
            ['Гостиная', 'gostinaya', 'Гостиной', 3],
            ['Детская', 'detskaya', 'Детской', 6],
            ['Спальня', 'spalnya', 'Спальной', 4],
            ['Балкон', 'balkon', 'Балкона', 7],
            ['Гардеробная', 'garderobnaya', 'Гардеробной', 9],
            ['Кабинет', 'kabinet', 'Кабинета', 11],
            ['Офис', 'ofis', 'Офиса', 8],
            ['Ландшафт', 'landshaft', 'Ландшафта', 10],
            ['Терраса', 'terrasa', 'Террасы', 13],
            ['Планировка помещений', 'planirovka', '"Планировка помещений"', 14],
            ['Прочее', 'prochee', 'Прочих помещений', 12],
        ];

        foreach ($rooms as $room) {
            $s = new Room();
            $s->name = $room[0];
            $s->slug = $room[1];
            $s->name_genitive = $room[2];
            $s->position = $room[3];
            $s->save();
        }
    }
}
