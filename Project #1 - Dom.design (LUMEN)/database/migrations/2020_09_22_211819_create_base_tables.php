<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBaseTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Создаем таблицу specializations, в которой будут храниться специализаций пользователей
        if (!Schema::hasTable('specializations')) {
            Schema::create('specializations', function (Blueprint $table) {
                $table->uuid('uuid')->primary();
                $table->string('name', 40)->unique();
                $table->softDeletes();
            });
        }

        // Создаем таблицу пользователей
        if (!Schema::hasTable('users')) {
            Schema::create('users', function (Blueprint $table) {
                $table->uuid('uuid')->primary();
                $table->string('name', 100)->nullable();
                $table->string('email', 100)->unique();
                $table->string('url_name', 50)->unique()->comment("The link that will be in the user profile url");
                $table->string('password', 100);
                $table->string('phone', 20)->nullable();
                $table->string('website', 60)->nullable();
                $table->string('address', 255)->nullable();
                $table->string('avatar_url', 100)->nullable();
                $table->string('avatar_sizes', 100)->nullable();
                $table->text('about')->nullable();
                $table->uuid('specialization_uuid')->nullable();
                $table->timestamp('email_verified_at')->nullable();
                $table->timestamps();
                $table->softDeletes();
                $table->foreign('specialization_uuid')->references('uuid')->on('specializations')->onDelete('set null')->onUpdate('cascade');
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('specializations');
    }
}
