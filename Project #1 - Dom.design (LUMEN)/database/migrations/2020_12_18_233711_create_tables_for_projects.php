<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class CreateTablesForProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Список тегов проекта
         */
        Schema::create('project_tags', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->string('name', 50)->unique();
            $table->integer('counter')->default(0);
        });

        /**
         * Список бюджетов
         */
        Schema::create('project_budgets', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->string('name', 50)->unique();
        });

        /**
         * Список стилей
         */
        Schema::create('project_styles', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->string('name', 50)->unique();
        });

        /**
         * Список помещений
         */
        Schema::create('rooms', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->string('name', 50)->unique();
            $table->string('name_genitive', 50);
            $table->string('slug', 50)->unique();
            $table->integer('position');
        });

        /**
         * Список групп цветов
         */
        Schema::create('color_groups', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->string('name', 50)->unique();
            $table->string('hex_code', 6)->unique();
        });

        /**
         * Список кодов цветов
         */
        Schema::create('colors', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->uuid('color_group_uuid');
            $table->string('hex_code', 6)->unique();
            $table->foreign('color_group_uuid')->references('uuid')->on('color_groups')->onDelete('cascade');
            $table->index(['color_group_uuid']);
        });

        /**
         * Список размеров помещений
         */
        Schema::create('room_sizes', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->string('name', 50)->unique();
        });

        /**
         * Информация о проекте
         */
        Schema::create('projects', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->uuid('user_uuid');
            $table->string('name', 100);
            $table->string('url_name', 150)->unique();
            $table->text('description')->nullable();
            $table->uuid('project_budget_uuid')->nullable();
            $table->uuid('project_style_uuid')->nullable();
            $table->integer('likes_counter')->default(0);
            $table->integer('views_counter')->default(0);
            $table->boolean('recommended')->default(false);
            $table->timestamp('verified_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_uuid')->references('uuid')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('project_budget_uuid')->references('uuid')->on('project_budgets')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('project_style_uuid')->references('uuid')->on('project_styles')->onDelete('set null')->onUpdate('cascade');
            $table->index(['user_uuid', 'name', 'project_budget_uuid', 'project_style_uuid', 'created_at', 'likes_counter', 'recommended']);
        });

        /**
         * Связь тегов и проектов
         */
        Schema::create('projects_project_tags', function (Blueprint $table) {
            $table->uuid('project_uuid');
            $table->uuid('project_tag_uuid');
            $table->foreign('project_uuid')->references('uuid')->on('projects')->onDelete('cascade');
            $table->foreign('project_tag_uuid')->references('uuid')->on('project_tags')->onDelete('cascade');
            $table->primary(['project_uuid', 'project_tag_uuid']);
        });

        /**
         * Связь проектов и лайков пользователя
         */
        Schema::create('project_likes', function (Blueprint $table) {
            $table->uuid('project_uuid');
            $table->uuid('user_uuid');
            $table->timestamp('created_at');
            $table->foreign('project_uuid')->references('uuid')->on('projects')->onDelete('cascade');
            $table->foreign('user_uuid')->references('uuid')->on('users')->onDelete('cascade');
            $table->primary(['project_uuid', 'user_uuid']);
        });

        /**
         * Сохраненные проекты пользователя
         */
        Schema::create('users_saved_projects', function (Blueprint $table) {
            $table->uuid('user_uuid');
            $table->uuid('project_uuid');
            $table->timestamp('created_at')->nullable();
            $table->foreign('user_uuid')->references('uuid')->on('users')->onDelete('cascade');
            $table->foreign('project_uuid')->references('uuid')->on('projects')->onDelete('cascade');
            $table->primary(['project_uuid', 'user_uuid']);
            $table->index(['created_at']);
        });

        /**
         * Фотограции проекта
         */
        Schema::create('project_images', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->string('url', 100);
            $table->string('sizes_name', 100);
            $table->string('width', 100);
            $table->string('height', 100);
            $table->uuid('project_uuid')->nullable();
            $table->uuid('room_uuid')->nullable();
            $table->uuid('room_size_uuid')->nullable();
            $table->integer('position')->default(0);
            $table->timestamps();
            $table->foreign('project_uuid')->references('uuid')->on('projects')->onDelete('cascade');
            $table->foreign('room_uuid')->references('uuid')->on('rooms')->onDelete('cascade');
            $table->foreign('room_size_uuid')->references('uuid')->on('room_sizes')->onDelete('cascade');
            $table->index(['project_uuid', 'room_uuid', 'room_size_uuid']);
        });

        /**
         * Связь фотографии проекта и цветов
         */
        Schema::create('project_images_colors', function (Blueprint $table) {
            $table->uuid('project_image_uuid');
            $table->uuid('color_uuid');
            $table->integer('position');
            $table->foreign('project_image_uuid')->references('uuid')->on('project_images')->onDelete('cascade');
            $table->foreign('color_uuid')->references('uuid')->on('colors')->onDelete('cascade');
            $table->primary(['project_image_uuid', 'color_uuid']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_images_colors');
        Schema::dropIfExists('project_images');
        Schema::dropIfExists('users_saved_projects');
        Schema::dropIfExists('project_likes');
        Schema::dropIfExists('projects_project_tags');
        Schema::dropIfExists('projects');
        Schema::dropIfExists('room_sizes');
        Schema::dropIfExists('colors');
        Schema::dropIfExists('color_groups');
        Schema::dropIfExists('rooms');
        Schema::dropIfExists('project_styles');
        Schema::dropIfExists('project_budgets');
        Schema::dropIfExists('project_tags');
    }
}
