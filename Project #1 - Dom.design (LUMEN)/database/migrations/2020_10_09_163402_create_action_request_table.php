<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActionRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('action_request')) {
            Schema::create('action_request', function (Blueprint $table) {
                $table->uuid('uuid')->primary();
                $table->string('action', 50);
                $table->string('hash', 50)->unique();
                $table->jsonb('params');
                $table->boolean('confirmed')->default(false);
                $table->timestamps();
                $table->index(['hash']);
             });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('action_request');
    }
}
