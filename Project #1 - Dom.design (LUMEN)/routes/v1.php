<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/*******************
 * User Controller
 ******************/
$router->get('/users', 'UserController@getUserList');
$router->get('/users/{uuid}', 'UserController@getUserInfoByUUID');
$router->post('/users', 'UserController@create');
$router->post('/users/avatar', 'UserController@uploadAvatar');
$router->patch('/users/{uuid}', ['middleware' => ['auth'], 'uses' => 'UserController@updateByUUID']);
$router->patch('/users/{uuid}/password/change', ['middleware' => ['auth'], 'uses' => 'UserController@updatePasswordByUUID']);
$router->delete('/users/{uuid}', ['middleware' => ['auth', 'role:super-admin'], 'uses' => 'UserController@softDelete']);

/*******************
 * Auth Controller
 ******************/
$router->post('/login', 'AuthController@login');
$router->post('/refresh', 'AuthController@refresh');
$router->get('/logout', 'AuthController@logout');
$router->get('/profile', ['middleware' => ['auth'], 'uses' => 'AuthController@profile']);

/*******************
 * Specialization Controller
 ******************/
$router->get('/specializations', 'SpecializationController@getSpecializationsList');
$router->get('/specializations/{uuid}', 'SpecializationController@getSpecializationByUUID');


/*******************
 * Action Request Controller
 ******************/
$router->post('/action/email/confirm/{hash}', 'ActionRequestController@confirmEmail');
$router->post('/action/password/reset/{hash}', 'ActionRequestController@resetPassword');


/*******************
 * Email Controller
 ******************/
$router->post('/email/password/reset', 'EmailController@sendMailToResetPassword');


/*******************
 * Project Controller
 ******************/
$router->get('/projects', 'ProjectController@getProjects');
$router->get('/project', 'ProjectController@getProject');
$router->post('/projects', ['middleware' => ['auth'], 'uses' => 'ProjectController@create']);
$router->patch('/projects/{uuid}', ['middleware' => ['auth'], 'uses' => 'ProjectController@update']);
$router->delete('/projects/{uuid}', ['middleware' => ['auth'], 'uses' => 'ProjectController@softDelete']);
$router->patch('/projects/{uuid}/like', ['middleware' => ['auth'], 'uses' => 'ProjectController@likeProject']);
$router->patch('/projects/{uuid}/unlike', ['middleware' => ['auth'], 'uses' => 'ProjectController@unlikeProject']);
$router->patch('/projects/{uuid}/save', ['middleware' => ['auth'], 'uses' => 'ProjectController@addToFavourites']);
$router->patch('/projects/{uuid}/unsave', ['middleware' => ['auth'], 'uses' => 'ProjectController@removeFromFavourites']);
$router->patch('/projects/{uuid}/view', 'ProjectController@increaseViewsCounter');


/*******************
 * Project Styles Controller
 ******************/
$router->get('/project-styles', 'ProjectStyleController@getProjectStylesList');


/*******************
 * Project Budget Controller
 ******************/
$router->get('/project-budgets', 'ProjectBudgetController@getProjectBudgetList');


/*******************
 * Rooms Controller
 ******************/
$router->get('/rooms', 'RoomController@getRoomsList');


/*******************
 * ColorGroup Controller
 ******************/
$router->get('/color-group', 'ColorGroupController@getColorGroupList');


/*******************
 * RoomSize Controller
 ******************/
$router->get('/room-size', 'RoomSizeController@getRoomSizeList');

/*******************
 * Project Images Controller
 ******************/
$router->post('/project-image', ['middleware' => ['auth'], 'uses' => 'ProjectImageController@create']);
$router->post('/project-image/update', ['middleware' => ['auth'], 'uses' => 'ProjectImageController@update']);

/*******************
 * Project Tags Controller
 ******************/
$router->get('/project-tags', 'ProjectTagsController@getProjectTags');

