<?php

return [

    /*
   |--------------------------------------------------------------------------
   | Параметры инвалижации кеша
   |--------------------------------------------------------------------------
   |
   | В нужном методе перечислить ключи кеша, при вызове этоко метода
   | кеш удалится по ключам
   |
   */
    'invalidateCache' => [
        'GET'    => [
            'UserController@getUserList'                       => [],
            'UserController@getUserInfoByUUID'                 => [],
            'ProjectController@getProjects'                    => [],
            'ProjectController@getProject'                     => [],
            'ProjectStyleController@getProjectStylesList'      => [],
            'ProjectBudgetController@getProjectBudgetList'     => [],
            'AuthController@logout'                            => [],
            'SpecializationController@getSpecializationsList'  => [],
            'SpecializationController@getSpecializationByUUID' => [],
            'AuthController@profile'                           => [],
            'RoomController@getRoomsList'                      => [],
            'RoomSizeController@getRoomSizeList'               => [],
            'ProjectTagsController@getProjectTags'             => [],
        ],
        'POST'   => [
            'UserController@create'                   => [],
            'UserController@uploadAvatar'             => [],
            'AuthController@login'                    => [],
            'AuthController@refresh'                  => [],
            'ActionRequestController@confirmEmail'    => [],
            'ActionRequestController@resetPassword'   => [],
            'EmailController@sendMailToResetPassword' => [],
            'ProjectController@create'                => [],
            'ProjectImageController@create'           => [],
            'ProjectImageController@update'           => [],
        ],
        'DELETE' => [
            'UserController@softDelete'    => [],
            'ProjectController@softDelete' => [],
        ],
        'PATCH'  => [
            'ProjectController@update'               => [],
            'UserController@updateByUUID'            => [],
            'UserController@updatePasswordByUUID'    => [],
            'ProjectController@likeProject'          => [],
            'ProjectController@unlikeProject'        => [],
            'ProjectController@addToFavourites'      => [],
            'ProjectController@removeFromFavourites' => [],
            'ProjectController@increaseViewsCounter' => [],
        ],
    ],
];



