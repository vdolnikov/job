<?php

return [

    'frontend_url' => env('FRONTEND_URL'),

    'google_captcha_secret' => env('GOOGLE_CAPTCHA_SECRET'),
    'google_captcha_unittest_secret' => env('GOOGLE_CAPTCHA_UNITTEST_SECRET'),

];
