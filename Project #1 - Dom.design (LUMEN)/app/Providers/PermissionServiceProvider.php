<?php

namespace App\Providers;

use App\Models\Permission;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\ServiceProvider;


class PermissionServiceProvider extends ServiceProvider
{
    /**
     * Register any auth services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any auth services.
     *
     * @return void
     */
    public function boot()
    {
        try {
            // Сделать для супер админа доступ на все
            Gate::before(function ($user, $ability) {
                if ($user->isSuperAdmin()) {
                    return true;
                }
            });

            Gate::define('change-self-data', function ($user, $data) {
                return $user->uuid === $data->uuid;
            });

            Gate::define('mail-confirmed', function ($user, $data) {
                return (boolean)$user->email_verified_at;
            });

            // Доступы из базы по ролям и пермишенам
            $this->permissionController();

        } catch (\Exception $e) {
            report($e);

            return false;
        }
    }


    /**
     * Здесь мы сопоставляем все Права, определяем slug Права (в нашем случае)
     * и проверяем, есть ли у Пользователя Право.
     * Теперь вы можете проверить Права Пользователя, как показано ниже.
     *
     * //вернёт true для текущего пользователя, если ему дано право управлять пользователями
     * Gate::allows('manage-users');
     */
    private function permissionController(){
        $permission = unserialize(Redis::get('table:permissions'));

        if (empty($permission)) {
            $permission = Permission::get();
            Redis::setex('table:permissions', 43200, serialize($permission));
        }

        $permission->map(function ($permission) {
            Gate::define($permission->slug, function ($user) use ($permission) {
                return $user->hasPermissionTo($permission);
            });
        });
    }
}
