<?php

namespace App\Traits;

use App\Models\Role;
use App\Models\Permission;

trait HasRolesAndPermissions
{
    /**
     * @return bool
     */
    public function isSuperAdmin()
    {
        return $this->hasRole(Role::MAX_PERMISSION_ROLE);
    }

    /**
     * @return mixed
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'users_roles');
    }

    /**
     * @return mixed
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'users_permissions');
    }

    /**
     * @param mixed ...$roles
     *
     * @return bool
     */
    public function hasRole(...$roles)
    {
        foreach ($roles as $role) {
            if ($this->roles->contains('slug', $role)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Метод проверяет если ли у залогиненого пользователя указанный пермишен
     *
     * Для проверки доступов на пользователе вместо этого метода
     * нужно использовать конструкцию ->can()
     *
     * @param $permission
     *
     * @return bool
     */
    public function hasPermission($permission)
    {
        return (bool)$this->permissions->where('slug', $permission)->count();
    }

    /**
     * Метод проверяет если ли у залогиненого пользователя указанный пермишен
     * Через роль и напрямую
     *
     * Для проверки доступов на пользователе вместо этого метода
     * нужно использовать конструкцию ->can()
     *
     * @param $permission
     *
     * @return bool
     */
    public function hasPermissionTo($permission)
    {
        return $this->hasPermissionThroughRole($permission) || $this->hasPermission($permission->slug);
    }

    /**
     * @param $permission
     *
     * @return bool
     */
    public function hasPermissionThroughRole($permission)
    {
        foreach ($permission->roles as $role) {
            if ($this->roles->contains($role)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Метод получает все Права на основе переданного массива
     *
     * @param array $permissions
     *
     * @return mixed
     */
    public function getAllPermissions(array $permissions)
    {
        return Permission::whereIn('slug', $permissions)->get();
    }

    /**
     * Даем разрешение на действие (пермишен) для пользователя
     *
     * @param mixed ...$permissions
     *
     * @return $this
     */
    public function givePermissionsTo(...$permissions)
    {
        $permissions = $this->getAllPermissions($permissions);
        if ($permissions === null) {
            //TODO Добавить вывод ошибки если пермишен не добавлен
            return $this;
        }
        $this->permissions()->saveMany($permissions);

        return $this;
    }

    /**
     * @param mixed ...$permissions
     *
     * @return $this
     */
    public function deletePermissions(...$permissions)
    {
        $permissions = $this->getAllPermissions($permissions);
        $this->permissions()->detach($permissions);

        return $this;
    }

    /**
     * @param mixed ...$permissions
     *
     * @return $this
     */
    public function refreshPermissions(...$permissions)
    {
        $this->permissions()->detach();

        return $this->givePermissionsTo($permissions);
    }

}
