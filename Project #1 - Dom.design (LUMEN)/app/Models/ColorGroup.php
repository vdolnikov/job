<?php

namespace App\Models;

use App\Traits\PrimaryKeyUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;

class ColorGroup extends Model
{
    use PrimaryKeyUuid;

    protected $table = "color_groups";

    private string $cacheKey = 'table:color_groups';

    private int $cacheExpire = 43200;

    public $timestamps = false;

    /**
     * Получаем список всех групп цветов из БД или кеша
     *
     * @return ColorGroup[]|\Illuminate\Database\Eloquent\Collection|mixed
     */
    public function getColorGroupList()
    {
        if ($colorGroupList = Redis::get($this->cacheKey)) {
            return json_decode($colorGroupList);
        }

        $colorGroupList = $this::all();

        Redis::setex($this->cacheKey, $this->cacheExpire, $colorGroupList);

        return $colorGroupList;
    }

}
