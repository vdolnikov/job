<?php

namespace App\Models;

use App\Traits\PrimaryKeyUuid;
use ColorThief\ColorThief;
use Illuminate\Database\Eloquent\Model;
use Imagick;

class Color extends Model
{
    use PrimaryKeyUuid;

    protected $table = "colors";

    public $timestamps = false;

    protected $fillable = [
        'hex_code',
        'color_group_uuid'
    ];

    public function projectImages()
    {
        return $this->belongsToMany(ProjectImage::class, 'project_images_colors');
    }

    /**
     * Получить палету цеветов изображения по пути файла
     *
     * @param     $ImagePath
     * @param int $colorCount
     * @param int $quality
     */
    public function getColorPaletteByImagePath($ImagePath, $colorCount = 10, $quality = 5)
    {
        $image = new Imagick();
        $image->readImage($ImagePath);

        return ColorThief::getPalette($image, $colorCount, $quality);
    }
}
