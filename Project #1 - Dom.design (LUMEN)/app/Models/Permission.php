<?php

namespace App\Models;


use App\Traits\PrimaryKeyUuid;
use Illuminate\Database\Eloquent\Model;


class Permission extends Model
{
    use PrimaryKeyUuid;

    protected $table = "permissions";

    protected $primaryKey = 'uuid';

    public $incrementing = false;

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'roles_permissions');
    }
}
