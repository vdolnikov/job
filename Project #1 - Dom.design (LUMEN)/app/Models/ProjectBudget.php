<?php

namespace App\Models;

use App\Traits\PrimaryKeyUuid;
use Illuminate\Database\Eloquent\Model;

class ProjectBudget extends Model
{
    use PrimaryKeyUuid;

    protected $table = "project_budgets";

    public $timestamps = false;
}
