<?php

namespace App\Models;

use App\Helper\ColorDiff;
use App\Models\FileUpload\ProjectImageFileUpload;
use App\Traits\PrimaryKeyUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProjectImage extends Model
{
    use PrimaryKeyUuid;

    protected $table = "project_images";

    protected $fillable = [
        'uuid',
        'url',
        'project_uuid',
        'room_uuid',
        'room_size_uuid',
        'position',
        'created_at',
        'updated_at',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function colors()
    {
        return $this->belongsToMany(Color::class, 'project_images_colors');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room()
    {
        return $this->belongsTo(Room::class, 'room_uuid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function roomSize()
    {
        return $this->belongsTo(RoomSize::class, 'room_size_uuid');
    }

    /**
     * Добавление изображений к проекту. В транзакции
     *
     * @param                      $project_uuid
     * @param                      $files
     * @param ProjectImageMetaData ...$filesMetaData
     *
     * @return mixed
     */
    public function addImagesInTransaction($project_uuid, $files, ProjectImageMetaData ...$filesMetaData): mixed
    {
        return DB::transaction(function () use ($project_uuid, $files, $filesMetaData) {
            return $this->addImages($project_uuid, $files, ...$filesMetaData);
        });
    }

    /**
     * Добавление изображений к проекту. Индекс файла соответствует индексу мето данных
     *
     * @param                      $project_uuid
     * @param                      $files            | Сам файл изображения
     * @param ProjectImageMetaData ...$filesMetaData | Данные по комнаты и размера комнаты на изображении:
     *                                               meta[0][room]: null
     *                                               meta[0][roomSize]: null
     *                                               meta[1][room]:{"uuid":"e392c728-8f07-49ac-98de-8d0381086535","name":"Спальня","slug":"spalnya","name_genitive":"Спальной","position":4}
     *                                               meta[1][roomSize]: {"uuid":"96f7d689-08ed-4ec8-929e-37812bf2cb38","name":"Средний"}
     *
     * @return bool
     * @throws \Throwable
     */
    public function addImages($project_uuid, $files, ProjectImageMetaData ...$filesMetaData)
    {
        $projectHash    = substr(str_replace(['-'], '', $project_uuid), 0, 12);
        $colorGroup     = new ColorGroup();
        $listColorGroup = $colorGroup->getColorGroupList();

        foreach ($files as $key => $file) {
            // 1) Определяем цвета на изображении
            $getPath        = $file->path();
            $color          = new Color();
            $imageColorsRBG = $color->getColorPaletteByImagePath($getPath);


            // 2) Добавляем цвета в БД и определяем группу цветов
            $colorArray    = [];
            $colorHEXArray = [];
            foreach ($imageColorsRBG as $colorRGB) {
                $colorHEX = ColorDiff::rgb2hex($colorRGB);
                if (in_array($colorHEX, $colorHEXArray)) {
                    continue;
                }

                $closeColorGroupUuid = ColorDiff::getCloseColorGroupByRGB($colorRGB, $listColorGroup)->uuid;
                $colorHEXArray[]     = $colorHEX;
                $colorArray[]        = Color::firstOrCreate(
                    ['hex_code' => $colorHEX],
                    ['color_group_uuid' => $closeColorGroupUuid]
                );
            }

            // 3) Генерируем пути до изображения и сохроняем в S3
            $fileUpload    = new ProjectImageFileUpload($file, $projectHash);
            $createdImages = $fileUpload->resizeAndSave();

            // 4) Обновляем БД. Добавляем запись изображения
            $projectImage                 = new ProjectImage();
            $projectImage->url            = $fileUpload->getFullFilePath();
            $projectImage->sizes_name     = implode(',', $createdImages['createdSizesName']);
            $projectImage->width          = implode(',', $createdImages['createdWidths']);
            $projectImage->height         = implode(',', $createdImages['createdHeights']);
            $projectImage->project_uuid   = $project_uuid;
            $projectImage->room_uuid      = $filesMetaData[$key]->room->uuid;
            $projectImage->room_size_uuid = $filesMetaData[$key]->roomSize->uuid;
            $projectImage->position       = $key;
            if (!$projectImage->saveOrFail()) {
                return false;
            };

            // 5) Связываем изображение с цветами
            foreach ($colorArray as $key => $color) {
                $projectImage->colors()->attach($color->uuid, [
                    'position' => $key,
                ]);
            }
        }

        return true;
    }

}
