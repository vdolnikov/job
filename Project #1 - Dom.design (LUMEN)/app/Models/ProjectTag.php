<?php

namespace App\Models;


use App\Traits\PrimaryKeyUuid;
use Illuminate\Database\Eloquent\Model;


class ProjectTag extends Model
{
    use PrimaryKeyUuid;

    protected $table = "project_tags";

    public $timestamps = false;

    protected $fillable = [
        'name',
    ];

    /**
     * Связь тегов и проектов
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function projects()
    {
        return $this->belongsToMany(Project::class, 'projects_project_tags');
    }

    /**
     * Приводим теги к общему формату
     *
     * @param array $project_tags
     *
     * @return array
     */
    public static function tagsFormatter(array $project_tags): array
    {
        $tags = [];

        foreach ($project_tags as $project_tag) {
            $tag            = mb_strtolower(trim($project_tag['name']));
            $tag            = preg_replace('/\s+/', ' ', $tag);
            $tags[]['name'] = $tag;
        }

        return $tags;
    }
}
