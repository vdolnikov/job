<?php

namespace App\Models;

use App\Traits\PrimaryKeyUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;

class RoomSize extends Model
{
    use PrimaryKeyUuid;

    protected $table = "room_sizes";

    private string $cacheKey = 'table:room_sizes';

    private int $cacheExpire = 43200;

    public $timestamps = false;

    /**
     * Получаем список всех размеров комнат
     * @return RoomSize[]|\Illuminate\Database\Eloquent\Collection|mixed
     */
    public function getRoomSizeList()
    {
        if ($RoomSizeList = Redis::get($this->cacheKey)) {
            return json_decode($RoomSizeList);
        }

        $RoomSizeList = $this::all();

        Redis::setex($this->cacheKey, $this->cacheExpire, $RoomSizeList);

        return $RoomSizeList;
    }

}
