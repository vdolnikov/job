<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ProjectLike extends Model
{
    protected $table = "project_likes";

    protected $primaryKey = null;

    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = [
        'project_uuid',
        'user_uuid',
        'created_at',
    ];

    /**
     * @param mixed $value
     *
     * @return ProjectLike
     */
    public function setCreatedAt($value): ProjectLike
    {
        return parent::setCreatedAt($value);
    }

}
