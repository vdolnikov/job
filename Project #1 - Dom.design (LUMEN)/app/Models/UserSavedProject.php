<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class UserSavedProject extends Model
{
    protected $table = "users_saved_projects";

    protected $primaryKey = null;

    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = [
        'user_uuid',
        'project_uuid',
        'created_at',
    ];

    /**
     * @param mixed $value
     *
     * @return ProjectLike
     */
    public function setCreatedAt($value): UserSavedProject
    {
        return parent::setCreatedAt($value);
    }

}
