<?php

namespace App\Models;

use App\Traits\HasRolesAndPermissions;
use App\Traits\PrimaryKeyUuid;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use Authenticatable, Authorizable, SoftDeletes, PrimaryKeyUuid, HasRolesAndPermissions;

    protected $table = "projects";

    protected $fillable = [
        'uuid',
        'user_uuid',
        'name',
        'url_name',
        'description',
        'project_budget_uuid',
        'project_style_uuid',
        'verified_at',
        'likes_counter',
        'views_counter',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'deleted_at',
    ];


    /**
     * Переводим названеи проетак в slug для URL
     *
     * @param $value
     */
    public function setUrlNameAttribute($value)
    {
        $random                       = mt_rand(100000, 999999);
        $this->attributes['url_name'] = $random . '-' . Str::slug($value);
    }

    /**
     * Добавляем теги для проекта, если тега не существует в таблице тегов то создаем его
     *
     * @param $project_tags
     *
     * @return bool
     */
    public function addTags($project_tags): bool
    {
        $oldTagsUuid = [];
        $newTagsName = ProjectTag::tagsFormatter($project_tags);

        $project_tags_from_db = DB::table('project_tags')->whereIn('name', $newTagsName)->get();

        foreach ($project_tags_from_db as $project_tag_from_db) {
            $oldTagsUuid[] = $project_tag_from_db->uuid;

            foreach ($newTagsName as $key => $newTagName) {
                if ($project_tag_from_db->name === $newTagName['name']) {
                    unset($newTagsName[$key]);
                }
            }
        }

        //Если тег существует связываем по найденному UUID
        $this->projectTags()->attach($oldTagsUuid);

        //Если тега нет то создаем его и потом всязываем с проектом
        $this->projectTags()->createMany($newTagsName);

        return true;
    }

    /**
     * Обновение тегов проекта
     * @param $project_uuid
     * @param $project_tags
     *
     * @return bool
     */
    public function updateTags($project_uuid, $project_tags): bool
    {
        if (empty($project_uuid)) {
            return false;
        }

        DB::table('projects_project_tags')->where('project_uuid', '=', $project_uuid)->delete();

        return $this->addTags($project_tags);
    }

    /**
     * Увязь проектов и тегов
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function projectTags()
    {
        return $this->belongsToMany(ProjectTag::class, 'projects_project_tags');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function projectUser()
    {
        return $this->belongsTo(User::class, 'user_uuid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function projectBudget()
    {
        return $this->belongsTo(ProjectBudget::class, 'project_budget_uuid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function projectStyle()
    {
        return $this->belongsTo(ProjectStyle::class, 'project_style_uuid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projectImages()
    {
        return $this->hasMany(ProjectImage::class, 'project_uuid')->orderBy('position');
    }

    public function projectLikes()
    {
        return $this->hasMany(ProjectLike::class, 'project_uuid');
    }

}
