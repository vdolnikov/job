<?php

namespace App\Models\FileUpload;


class ProjectImageFileUpload extends ImageFileUpload
{
    /**
     * Корневой каталог
     */
    const ROOT_FOLDER = 'projects';

    /**
     * @var string
     */
    public string $rootFolder = self::ROOT_FOLDER;

    /**
     * @var array|array[]
     */
    protected array $fileSizes = [
        [
            'sizeName' => self::ORIGINAL,
            'width'    => null,
            'height'   => null,
            'quality'  => 80,
        ],
        [
            'sizeName' => '160w',
            'width'    => 160,
            'height'   => null,
            'quality'  => 70,
        ],
        [
            'sizeName' => '320w',
            'width'    => 320,
            'height'   => null,
            'quality'  => 70,
        ],
        [
            'sizeName' => '480w',
            'width'    => 480,
            'height'   => null,
            'quality'  => 70,
        ],
        [
            'sizeName' => '640w',
            'width'    => 640,
            'height'   => null,
            'quality'  => 70,
        ],
        [
            'sizeName' => '960w',
            'width'    => 960,
            'height'   => null,
            'quality'  => 70,
        ],
        [
            'sizeName' => '1280w',
            'width'    => 1280,
            'height'   => null,
            'quality'  => 70,
        ],
        [
            'sizeName' => '1920w',
            'width'    => 1920,
            'height'   => null,
            'quality'  => 70,
        ],
    ];

}
