<?php

namespace App\Models\FileUpload;


class AvatarFileUpload extends ImageFileUpload
{
    /**
     * Корневой каталог
     */
    const ROOT_FOLDER = 'avatars';


    /**
     * @var string
     */
    public string $rootFolder = self::ROOT_FOLDER;

    /**
     * @var array[]
     */
    protected array $fileSizes = [
        [
            'sizeName' => 'w40',
            'width'    => 40,
            'height'   => 40,
            'quality'  => 70,
        ],
        [
            'sizeName' => 'w300',
            'width'    => 300,
            'height'   => 300,
            'quality'  => 90,
        ],
    ];
}
