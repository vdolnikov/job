<?php

namespace App\Models\FileUpload;

use Illuminate\Http\UploadedFile;

class FileUpload
{
    /**
     * Где хранить все файлы
     */
    const STORAGE = 's3';

    /**
     * Корневой каталог
     */
    const ROOT_FOLDER = 'other';
    
    /**
     * @var string
     */
    public string $rootFolder = self::ROOT_FOLDER;

    /**
     * @var UploadedFile null
     */
    public UploadedFile $file;

    /**
     * $hash = Параметр по которому строится путь к файлу: e392c7288f07 => $rootFolder/e3/92/e392c7288f07
     * Если нужно чтобы несколько файлов хранились в одном каталоге, то нужно передавать hash из вне
     * Иначе фалы будут храниться в разных каталогах
     *
     * Нр. в "изображениях проектов" я генерировал hash так: substr(str_replace(['-'], '', $project_uuid), 0, 12);
     * Это позволяет зная uuid проекта быстро найти папку с фотографиями проекта
     */
    protected string $hash;

    /**
     * @var string
     */
    protected string $filePath;

    /**
     * @var string
     */
    protected string $fileName;

    /**
     * FileUpload constructor.
     * FileUpload constructor.
     * $hash - Пусть файла, передавать из вне если нужно чтобы несколько файлов лежали в одном каталоге
     * $hashFormUUID - Генерирует хеш (путь к файлу) из переданного uuid
     *
     * @param UploadedFile $file
     * @param null         $userHash
     * @param null         $hashFormUUID
     */
    public function __construct(UploadedFile $file, $userHash = null, $hashFormUUID = null)
    {
        $this->file = $file;
        $this->setHash($userHash, $hashFormUUID);
        $this->setFilePath();
        $this->setFileName();
    }

    /**
     * @param null $userHash
     * @param null $hashFormUUID
     */
    private function setHash($userHash = null, $hashFormUUID = null)
    {
        if ($userHash !== null) {
            $this->hash = $userHash;

            return;
        }

        if ($hashFormUUID !== null) {
            $this->hash = substr(str_replace(['-'], '', $hashFormUUID), 0, 12);

            return;
        }

        $this->hash = uniqid();
    }

    /**
     * Генрируем имя для файла, по самому файлу
     *
     * @param int $fileNameLength
     */
    public function setFileName($fileNameLength = 12)
    {
        $this->fileName = substr(md5($this->file), 0, $fileNameLength) . '.' . $this->file->guessExtension();
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * Генерируем путь до файла
     *
     * Максимальная величина вложенности:
     * rootFolder/16^2 (256 каталогов)/16^2 (256 каталогов)/~256(макс. но может быть и больще) = 65536 (каталогов) * 256 =  минимуи может вместить 16.777.216
     */
    public function setFilePath()
    {
        $dir1 = substr($this->hash, 0, 2);
        $dir2 = substr($this->hash, 2, 2);

        $this->filePath = $this->rootFolder . '/' . $dir1 . '/' . $dir2 . '/' . $this->hash;
    }

    /**
     * @return string
     */
    public function getFilePath(): string
    {
        return $this->fileName;
    }

    /**
     * @return string
     */
    public function getFullFilePath(): string
    {
        return $this->filePath . '/' . $this->fileName;
    }

    /**
     * @return string
     */
    public function getRootFolder(): string
    {
        return $this->rootFolder;
    }

    /**
     * @return false|string FilePath
     */
    public function save(): bool|string
    {
        return $this->file->storeAs($this->filePath, $this->fileName, self::STORAGE);
    }


}
