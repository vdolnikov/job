<?php

namespace App\Models\FileUpload;


use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

abstract class ImageFileUpload extends FileUpload
{
    const SIZE_TMP   = '{SIZE}';
    const ORIGINAL   = 'original';
    const MAX_WIDTH  = 5000;
    const MAX_HEIGHT = 5000;
    const MAX_SIZE   = 5242880; //5МБ


    /**
     * Генерация пути до файла
     * Переопределяем метод setFilePath, так как для изображений мы еще будем разделять пусть на размеры изображений
     */
    public function setFilePath()
    {
        $dir1 = substr($this->hash, 0, 2);
        $dir2 = substr($this->hash, 2, 2);

        $this->filePath = $this->rootFolder . '/' . self::SIZE_TMP . '/' . $dir1 . '/' . $dir2 . '/' . $this->hash;
    }

    /**
     * @return array
     */
    public function getFileSizes(): array
    {
        return $this->fileSizes;
    }

    /**
     * Создает изображения в разных размерах
     * И возвращает список созданных размеров
     *
     * @return array
     * @throws \Exception
     */
    public function resizeAndSave(): array
    {
        // 1) Инициализируем изображение
        $newImage = Image::make($this->file);
        $newImage->backup();

        // 2) Не даем сохранить слишком большие изображения
        if ($newImage->getWidth() > self::MAX_WIDTH ||
            $newImage->getHeight() > self::MAX_HEIGHT ||
            $newImage->filesize() > self::MAX_SIZE
        ) {
            throw new \Exception('Fail to load file to Amazon S3! File too large!');
        }

        $createdSizesName = [];
        $createdWidths    = [];
        $createdHeights   = [];
        foreach ($this->fileSizes as $fileSize) {
            // 3) Не меняем если это оригинал
            if ($fileSize['sizeName'] !== self::ORIGINAL) {
                $newImage->resize($fileSize['width'], $fileSize['height'], function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            }

            $newImage->encode($this->file->guessExtension(), $fileSize['quality']);
            $filledFilePath = str_replace(AvatarFileUpload::SIZE_TMP, $fileSize['sizeName'], $this->getFullFilePath());

            if (!$this->saveChangedImage($newImage, $filledFilePath)) {
                throw new \Exception('Fail to load file to Amazon S3');
            }

            $createdSizesName[] = $fileSize['sizeName'];
            $createdWidths[]    = $newImage->width();
            $createdHeights[]   = $newImage->height();
            $newImage->reset();
        }

        return ['createdSizesName' => $createdSizesName, 'createdWidths' => $createdWidths, 'createdHeights' => $createdHeights];
    }

    /**
     * Сохранение изображений
     *
     * @return false|string FilePath
     */
    protected function saveChangedImage($image, $filePath): bool|string
    {
        return Storage::disk(self::STORAGE)->put($filePath, (string)$image, 'public');
    }

    /**
     * Удаляем фалы изображений (Все размеры)
     * $pattern = $rootFolder/e3/92/e392c7288f07/a7719af1bcf07.jpg
     *
     * @param string $rootFolder
     * @param string $filePathTemplate
     * @param array  $sizesName
     *
     * @return bool
     */
    static function deleteImagesFileFromS3(string $rootFolder, string $filePathTemplate, array $sizesName): bool
    {
        $pattern       = '/' . $rootFolder . '\/.*\/..\/..\/.*\/.*\..*/';
        $pathIsCorrect = preg_match($pattern, $filePathTemplate);

        if ($pathIsCorrect) {
            foreach ($sizesName as $size) {
                $filePath = str_replace(self::SIZE_TMP, $size, $filePathTemplate);

                if (!Storage::disk('s3')->delete($filePath)) {
                    Log::error('Fail to delete old file in Amazon S3: ' . $filePath);

                    return false;
                }
            }
        } else {
            Log::error('Fail to delete old file in Amazon S3 (incorrect path): ' . $filePathTemplate);

            return false;
        }

        return true;
    }
}
