<?php

namespace App\Models;


use App\Traits\PrimaryKeyUuid;
use Illuminate\Database\Eloquent\Model;


class Role extends Model
{
    use PrimaryKeyUuid;

    const MAX_PERMISSION_ROLE = "super-admin";
    const BUSINESS_ROLE       = "business";
    const DEFAULT_ROLE        = "user";

    protected $table = "roles";

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'pivot',
    ];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'roles_permissions');
    }
}
