<?php

namespace App\Models;

use App\Traits\PrimaryKeyUuid;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    use PrimaryKeyUuid;

    protected $table = "rooms";

    public $timestamps = false;
}
