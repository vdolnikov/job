<?php

namespace App\Models;

use App\Traits\PrimaryKeyUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Specialization extends Model
{
    use SoftDeletes, PrimaryKeyUuid;

    protected $table = "specializations";

    public $timestamps = false;

    protected $hidden = [
        'deleted_at',
    ];
}
