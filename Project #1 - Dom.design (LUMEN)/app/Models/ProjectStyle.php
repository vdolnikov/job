<?php

namespace App\Models;

use App\Traits\PrimaryKeyUuid;
use Illuminate\Database\Eloquent\Model;

class ProjectStyle extends Model
{
    use PrimaryKeyUuid;

    protected $table = "project_styles";

    public $timestamps = false;

}
