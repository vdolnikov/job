<?php


namespace App\Models;


class ProjectImageMetaData
{
    /**
     * @var Room
     */
    public $room = null;

    /**
     * @var RoomSize
     */
    public $roomSize = null;


    /**
     * ProjectImageMetaData constructor.
     * meta[0][room]: null
     * meta[0][roomSize]: null
     * meta[1][room]:{"uuid":"e392c728-8f07-49ac-98de-8d0381086535","name":"Спальня","slug":"spalnya","name_genitive":"Спальной","position":4}
     * meta[1][roomSize]: {"uuid":"96f7d689-08ed-4ec8-929e-37812bf2cb38","name":"Средний"}
     *
     * @param $data
     */
    public function __construct($data = [])
    {
        if (!empty($data['room'])) {
            $dataRoom = json_decode($data['room']);
        }

        $room                = new Room();
        $room->uuid          = $dataRoom->uuid ?? null;
        $room->name          = $dataRoom->name ?? null;
        $room->slug          = $dataRoom->slug ?? null;
        $room->name_genitive = $dataRoom->name_genitive ?? null;
        $room->position      = $dataRoom->position ?? null;
        $this->room          = $room;


        if (!empty($data['roomSize'])) {
            $dataRoomSize = json_decode($data['roomSize']);
        }

        $roomSize       = new RoomSize();
        $roomSize->uuid = $dataRoomSize->uuid ?? null;
        $roomSize->name = $dataRoomSize->name ?? null;
        $this->roomSize = $roomSize;

        return $this;
    }

}
