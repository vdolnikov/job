<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


class ResetPasswordMail extends Mailable
{
    use Queueable, SerializesModels;

    public $hash;

    public function __construct($hash)
    {
        $this->hash = $hash;
    }

    public function build()
    {
        $subject = 'Обновить пароль на Dom.Design';

        return $this->view('emails.ResetPassword')
            ->subject($subject)
            ->with([
                'data' => $this->hash,
            ]);
    }
}
