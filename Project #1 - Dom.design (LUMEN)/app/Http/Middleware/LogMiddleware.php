<?php

namespace App\Http\Middleware;


use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

final class LogMiddleware
{

    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);

        $statusCode  = $response->getStatusCode();
        $firstNumber = substr($statusCode, 0, 1);
        switch ($firstNumber) {
            case (5):
                $logName = "ERROR";
                break;
            case (4):
                $logName = "WARNING";
                break;
            default:
                $logName = "INFO";
        }

        $url = $request->server()['APP_URL'] ?? '';
        Log::write($logName, "API_QUERY | " . $statusCode . " " . $request->server()['REQUEST_METHOD'] . " | " . $url . '/' . $request->path() . "; " .
//            "Headers: " . json_encode($request->headers->all()) . "; " .
            "Request: " . json_encode($request->all()) . "; " .
            "Response: " . json_encode($response) . "; ");

        return $response;
    }
}
