<?php

namespace App\Http\Middleware;

use Closure;


use Illuminate\Http\Request;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;


class InvalidateCache
{


    public function handle(Request $request, Closure $next)
    {

        $response = $next($request);
        $route    = $request->route();
        $method   = $request->method();

        try {
            $routeArr            = explode('\\', $route[1]['uses']);
            $controllerMethodKey = end($routeArr);

            $config    = config('cacheParams.invalidateCache');
            $cacheKeys = $config[$method][$controllerMethodKey];

            foreach ($cacheKeys as $cacheKey) {
                Redis::del($cacheKey);
            }

        } catch (\ErrorException $e) {
            Log::critical('Метод: ' . $route[1]['uses'] . ' не добавлен в список на инвалидацию см. config/cacheParams.php');
        }


        return $response;
    }
}
