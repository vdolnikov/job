<?php

namespace App\Http\Controllers\v1;

use App\Models\Specialization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

class SpecializationController extends Controller
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSpecializationsList()
    {
        if ($specializations = Redis::get('table:specializations')) {
            return response()->json(json_decode($specializations));
        }

        $specializations = Specialization::all();

        Redis::setex('table:specializations', 43200, $specializations);

        return response()->json($specializations);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSpecializationByUUID($uuid, Request $request)
    {
        $request['uuid'] = $uuid;
        $this->validate($request, [
            'uuid' => 'uuid',
        ]);

        try {
            $userInfo = Specialization::where('uuid', $uuid)->firstOrFail();

            return response()->json($userInfo);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Fail to get specialization info'], 500);
        }
    }


}
