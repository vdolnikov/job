<?php

namespace App\Http\Controllers\v1;

use App\Models\ActionRequest;
use App\Models\FileUpload\AvatarFileUpload;
use App\Models\FileUpload\ImageFileUpload;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Получить список пользователей
     * Или конкретного пользователя по uuid, email, url_name
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getUserList(Request $request)
    {
        $this->validate($request, [
            'email'    => 'email|max:100',
            'uuid'     => 'uuid',
            'url_name' => 'string',
        ]);

        try {
            if (isset($request->uuid)) {
                $users = User::where('uuid', $request->uuid)->with('specialization', 'roles:slug')->firstOrFail();
            } elseif (isset($request->email)) {
                $users = User::where('email', $request->email)->with('specialization', 'roles:slug')->firstOrFail();
            } elseif (isset($request->url_name)) {
                $users = User::where('url_name', $request->url_name)->with('specialization', 'roles:slug')->firstOrFail();
            } else {
                $users = User::with('specialization', 'roles:slug')->simplePaginate(20);
            }

            return response()->json($users);
        } catch (ModelNotFoundException $e) {
            Log::error($e->getMessage(), $e->getIds());

            return response()->json(['success' => false, 'error' => 'User not found'], 404);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Fail to get user info'], 500);
        }
    }

    /**
     * Получить информацию о пользователе по UUID
     *
     * @param $uuid
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getUserInfoByUUID($uuid, Request $request)
    {
        $request['uuid'] = $uuid;
        $this->validate($request, [
            'uuid' => 'uuid',
        ]);

        try {
            $userInfo = User::where('uuid', $uuid)->with('specialization', 'roles:slug')->firstOrFail();

            return response()->json($userInfo);
        } catch (ModelNotFoundException $e) {
            Log::error($e->getMessage(), $e->getIds());

            return response()->json(['success' => false, 'error' => 'User not found'], 404);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Fail to get user info'], 500);
        }
    }

    /**
     * Добавление нового пользователя
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Throwable
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'name'                  => 'string|nullable|max:100',
            'email'                 => 'required|email|unique:users|max:100',
            'password'              => 'required|min:5|max:100',
            'password_confirmation' => 'required|same:password',
            'isBusiness'            => 'boolean',
            'specialization_uuid'   => 'uuid',
            'token'                 => 'required|string',
        ]);

        if (!$this->checkGoogleRecaptchaToken($request->token)) {
            return response()->json(['message' => 'You are robot'], 429);
        }

        try {
            $user           = new User();
            $user->name     = strip_tags($request->name);
            $user->email    = $request->email;
            $user->password = $request->password;
            $user->url_name = mt_rand(10000000000, 99999999999);
            //todo добавить проверку на существование такой специальности (работает пока у поля есть forign key)
            $user->specialization_uuid = $request->specialization_uuid;
            $user->saveOrFail();

            $roleName = !empty($request->isBusiness) ? Role::BUSINESS_ROLE : Role::DEFAULT_ROLE;
            $role     = Role::where('slug', $roleName)->first();
            $user->roles()->attach($role);

            // Отправляем письмо подтверждения email и создание события на подтверждение емейла
            (new ActionRequest())->createConfirmEmailAction($user->email);

        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => ['Fail to add new user']], 500);
        }

        return response()->json($user, 201);
    }

    /**
     * Обновление информации о пользователе по UUID
     *
     * @param         $uuid
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function updateByUUID($uuid, Request $request)
    {
        $request['uuid'] = $uuid;

        $this->validate($request, [
            'uuid'                => 'uuid',
            'name'                => 'string|filled|min:3|max:100',
            'url_name'            => 'string|filled|unique:users|min:5|max:50|regex:/^[a-z0-9_]+$/',
            'phone'               => 'string|min:5|max:20',
            'website'             => 'string|min:5|max:60',
            'address'             => 'string|min:5|max:255',
            'about'               => 'string|min:20|max:500',
            'specialization_uuid' => 'uuid',
        ]);

        $availableToEditParams = $request->only(['name', 'url_name', 'phone', 'website', 'address', 'about', 'specialization_uuid']);

        //TODO Сделать валидацию для specialization_uuid на существование такого значения в БД

        try {
            $user = User::findOrFail($uuid);

            if (!$request->user()->can('change-self-data', $user)) {
                return response()->json(['success' => false, 'error' => 'Access denied for user'], 403);
            }

            $user->update($availableToEditParams);
        } catch (ModelNotFoundException $e) {
            Log::error($e->getMessage(), $e->getIds());

            return response()->json(['success' => false, 'error' => 'User not found'], 404);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Fail to update user'], 500);
        }

        return response()->json($user, 200);
    }

    /**
     * Обновление пололя пользователя
     *
     * @param         $uuid
     * @param Request $request
     */
    public function updatePasswordByUUID($uuid, Request $request)
    {
        $request['uuid'] = $uuid;

        $rules = [
            'uuid'                  => 'uuid',
            'password'              => 'required|min:5|different:password_old',
            'password_confirmation' => 'required|same:password',
            'password_old'          => 'required',
        ];

        try {
            $user = User::findOrFail($uuid);

            if (!$request->user()->can('change-self-data', $user)) {
                return response()->json(['success' => false, 'error' => 'Access denied for user'], 403);
            }

            $validator = Validator::make($request->all(), $rules);

            $validator->after(function ($validator) use ($user, $request) {
                if (!Hash::check($request->password_old, $user->password)) {
                    $validator->errors()->add('password_old', 'Old password does not match');
                }
            });

            if ($validator->fails()) {
                return response($validator->errors(), 422);
            }

            $status = $user->update(['password' => $request->password]);
            if (!$status) {
                throw new \Exception('Fail to update user');
            };

            return response()->json(['success' => true]);

        } catch (ModelNotFoundException $e) {
            Log::error($e->getMessage(), $e->getIds());

            return response()->json(['success' => false, 'error' => 'User not found'], 404);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Fail to update user'], 500);
        }
    }

    /**
     * Удаление пользователя с проставлением флага
     *
     * @param         $uuid
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function softDelete($uuid, Request $request)
    {
        $request['uuid'] = $uuid;

        $validator = Validator::make($request->all(), [
            'uuid' => 'uuid',
        ]);

        try {
            $user = User::findOrFail($request->uuid);
            $user->delete();

            return response()->json(['success' => true]);
        } catch (ModelNotFoundException $e) {
            Log::error($e->getMessage(), $e->getIds());

            return response()->json(['success' => false, 'error' => 'User not found'], 404);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Fail to delete user'], 500);
        }
    }

    /**
     * Метод добавляет аватарку пользователя и сохраняет файл в S3
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function uploadAvatar(Request $request)
    {
        $this->validate($request, [
            'uuid' => 'required|uuid',
            'file' => 'required|mimes:jpeg,png|max:200',
        ]);

        try {

            $filePathTemplate = DB::transaction(function () use ($request) {
                $user                = User::findOrFail($request->uuid);
                $oldFilePathTemplate = $user->avatar_url;
                $oldFileSizes        = explode(',', $user->avatar_sizes);

                if (!$request->user()->can('change-self-data', $user)) {
                    return response()->json(['success' => false, 'error' => 'Access denied for user'], 403);
                }


                $avatarFileUpload = new AvatarFileUpload($request->file('file'), null, $user->uuid);
                $filePathTemplate = $avatarFileUpload->getFullFilePath();

                // Add new files to S3
                $createdImages = $avatarFileUpload->resizeAndSave();

                // Update url in DB
                if (!$user->update(['avatar_url' => $filePathTemplate, 'avatar_sizes' => implode(',', $createdImages['createdSizesName'])])) {
                    throw new \Exception('Fail to update user avatar url in DB');
                };

                // Delete old file form S3
                if (!empty($oldFilePathTemplate) && $oldFilePathTemplate !== $filePathTemplate) {
                    ImageFileUpload::deleteImagesFileFromS3($avatarFileUpload->rootFolder, $oldFilePathTemplate, $oldFileSizes);
                }

                return $filePathTemplate;

            });

            return response()->json(['success' => true, 'path' => $filePathTemplate], 200);

        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Fail to upload user avatar'], 500);
        }
    }
}
