<?php

namespace App\Http\Controllers\v1;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;


class RoomController extends Controller
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRoomsList()
    {
        if ($rooms = Redis::get('table:rooms')) {
            return response()->json(json_decode($rooms));
        }

        $rooms = DB::table('rooms')->orderBy('position')->get();

        Redis::setex('table:rooms', 43200, $rooms);

        return response()->json($rooms);
    }

}
