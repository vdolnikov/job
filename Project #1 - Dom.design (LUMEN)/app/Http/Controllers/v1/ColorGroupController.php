<?php

namespace App\Http\Controllers\v1;

use App\Models\ColorGroup;

class ColorGroupController extends Controller
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getColorGroupList()
    {
        $colorGroup = new ColorGroup();
        $response = $colorGroup->getColorGroupList();

        return response()->json($response);
    }

}
