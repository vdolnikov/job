<?php

namespace App\Http\Controllers\v1;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;


class AuthController extends Controller
{
    /**
     * Получить профиль авторизованного пользователя
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function profile()
    {

        $authUser = JWTAuth::user();
        $user     = User::where('uuid', $authUser->uuid)->with('specialization', 'roles:slug')->firstOrFail();


        return response()->json(['user' => $user], 200);
    }


    /**
     * @param Request $request
     *  //TODO Добавить обработку ошибок в норамльном виде (чтобы понимать почему пользователь не смог авторизироваться)
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'email'    => 'required|email',
            'password' => 'required|string',
            'token'    => 'required',
        ]);


        if (!$this->checkGoogleRecaptchaToken($request->token)) {
            return response()->json(['message' => 'You are robot'], 429);
        }

        $credentials = $request->only(['email', 'password']);
        if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json(['message' => 'Authorisation Error'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Очистка токенов
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        try {
            auth()->logout();

            return response()->json(['success' => true]);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()], 500);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        try {
            return $this->respondWithToken(auth()->refresh(true, true));
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'error' => $e->getMessage()], 500);
        }
    }

    protected function respondWithToken($token)
    {
        return response()->json([
            'token'      => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::factory()->getTTL() * 60,
        ], 200);
    }
}
