<?php

namespace App\Http\Controllers\v1;

use App\Models\ActionRequest;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;


class EmailController extends Controller
{
    /**
     * Отправлка ссылки на сброс пароля
     *
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function sendMailToResetPassword(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'token' => 'required|string',
        ]);

        if (!$this->checkGoogleRecaptchaToken($request->token)) {
            return response()->json(['message' => 'You are robot'], 429);
        }

        try {
            $user = User::where('email', $request->email)->firstOrFail();
            (new ActionRequest())->createResetPasswordAction($user->email);
        } catch (ModelNotFoundException $e) {
            Log::error($e->getMessage(), $e->getIds());

            return response()->json(['success' => false, 'error' => 'User not found'], 404);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Fail to send reset password email'], 500);
        }

        return response()->json(['success' => true], 200);
    }
}
