<?php

namespace App\Http\Controllers\v1;

use App\Models\ProjectStyle;
use Illuminate\Support\Facades\Redis;


class ProjectStyleController extends Controller
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProjectStylesList()
    {
        if ($projectStyles = Redis::get('table:project_styles')) {
            return response()->json(json_decode($projectStyles));
        }

        $projectStyles = ProjectStyle::all();

        Redis::setex('table:project_styles', 43200, $projectStyles);

        return response()->json($projectStyles);
    }

}
