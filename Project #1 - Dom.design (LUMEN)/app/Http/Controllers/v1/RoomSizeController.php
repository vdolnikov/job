<?php

namespace App\Http\Controllers\v1;

use App\Models\RoomSize;


class RoomSizeController extends Controller
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRoomSizeList()
    {

        $roomSizeList = new RoomSize();
        $response = $roomSizeList->getRoomSizeList();

        return response()->json($response);
    }

}
