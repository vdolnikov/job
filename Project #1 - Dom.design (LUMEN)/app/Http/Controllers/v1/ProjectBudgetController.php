<?php

namespace App\Http\Controllers\v1;

use App\Models\ProjectBudget;
use Illuminate\Support\Facades\Redis;


class ProjectBudgetController extends Controller
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProjectBudgetList()
    {
        if ($projectBudget = Redis::get('table:project_budgets')) {
            return response()->json(json_decode($projectBudget));
        }

        $projectBudget = ProjectBudget::all();

        Redis::setex('table:project_budgets', 43200, $projectBudget);

        return response()->json($projectBudget);
    }

}
