<?php

namespace App\Http\Controllers\v1;

use App\Models\Project;
use App\Models\ProjectLike;
use App\Models\User;
use App\Models\UserSavedProject;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use Mockery\Exception;

class ProjectController extends Controller
{

    /**
     * Создаение проекта
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Throwable
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'name'                => 'required|min:2|max:100',
            'description'         => 'string|min:3|max:500',
            'project_budget_uuid' => 'nullable|uuid',
            'project_style_uuid'  => 'nullable|uuid',
            'project_tags'        => 'nullable|array',
        ]);


        try {

            if (!$request->user()->can('add-project', $request->user())) {
                return response()->json(['success' => false, 'error' => 'Access denied for user'], 403);
            }

            $project = DB::transaction(function () use ($request) {
                $project                      = new Project();
                $project->name                = $request->name;
                $project->url_name            = $request->name;
                $project->project_budget_uuid = $request->project_budget_uuid;
                $project->project_style_uuid  = $request->project_style_uuid;
                $project->user_uuid           = $request->user()->uuid;
                $project->description         = $request->description;
                $project->saveOrFail();

                $project->addTags($request->project_tags);

                return $project;
            });

            return response()->json([
                'success' => true,
                'uuid'    => $project->uuid,
            ], 200);

        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => ['Fail to create project']], 500);
        }

    }

    /**
     * Обновление проекта
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Throwable
     */
    public function update($uuid, Request $request)
    {
        $request['uuid'] = $uuid;

        $this->validate($request, [
            'uuid'                => 'required|uuid',
            'name'                => 'required|min:2|max:100',
            'description'         => 'string|min:3|max:500',
            'project_budget_uuid' => 'nullable|uuid',
            'project_style_uuid'  => 'nullable|uuid',
            'project_tags'        => 'nullable|array',
        ]);


        try {

            if (!$request->user()->can('add-project', $request->user())) {
                return response()->json(['success' => false, 'error' => 'Access denied for user'], 403);
            }

            DB::transaction(function () use ($request) {

                $project = Project::findOrFail($request->uuid);

                if ($project->user_uuid !== $request->user()->uuid) {
                    return response()->json(['success' => false, 'error' => 'Access denied for user'], 403);
                }

                $project->name                = $request->name;
                $project->description         = $request->description;
                $project->project_budget_uuid = $request->project_budget_uuid;
                $project->project_style_uuid  = $request->project_style_uuid;
                $project->verified_at         = null;
                $project->saveOrFail();

                $project->updateTags($request->uuid, $request->project_tags);

                return true;

            });

            return response()->json([
                'success' => true,
                'uuid'    => $request->uuid,
            ], 200);


        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => ['Fail to create project']], 500);
        }

    }

    /**
     * Удаление прокета
     *
     * @param         $uuid
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function softDelete($uuid, Request $request)
    {
        $request['uuid'] = $uuid;

        $this->validate($request, [
            'uuid' => 'required|uuid',
        ]);


        try {
            $project    = Project::findOrFail($request->uuid);
            $user       = new User();
            $user->uuid = $project->user_uuid;

            //TODO сделать мидлвар и вынести туда проверку что данный метод может вызывать только владелец
            if (!$request->user()->can('change-self-data', $user)) {
                return response()->json(['success' => false, 'error' => 'Access denied for user'], 403);
            }

            if ($project->delete()) {
                return response()->json(['success' => true]);
            }
        } catch (ModelNotFoundException $e) {
            Log::error($e->getMessage(), $e->getIds());

            return response()->json(['success' => false, 'error' => 'Project not found'], 404);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Fail to delete project'], 500);
        }
    }

    /**
     * Метод для вывода списка проектов
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getProjects(Request $request)
    {
        $this->validate($request, [
            'user_uuid'       => 'uuid',
            'per_page'        => 'numeric|min:1|max:100',
            'search_text'     => 'string|min:2',
            'order_by'        => 'string|min:2|in:created_at',
            'image_room_uuid' => 'uuid',
            'saved_projects'  => 'boolean',
        ]);

        try {

            $request->per_page = !empty($request->per_page) ? $request->per_page : 10;

            // 0) Проверяем является ли пользователь владельцем проекта
            // Переданный user_uuid
            $isUserProjectOwner = false;
            if (!empty($request->user_uuid)) {
                $user       = new User();
                $user->uuid = $request->user_uuid;

                //Пользователь авторизован и переданный user_uuid == авторизованному
                if (!is_null($request->user()) && $request->user()->can('change-self-data', $user)) {
                    $isUserProjectOwner = true;
                }
            }


            $projects = DB::table('projects')
                ->select(
                    'projects.uuid as project_uuid',
                    'projects.name as project_name',
                    'projects.url_name as project_url',
                    'projects.likes_counter',
                    'projects.views_counter',
                    'users.name as user_name',
                    'users.url_name as user_url',
                    'users.avatar_url',
                    'projects.verified_at as verified_at'
                );


            $projects->leftJoin('users', 'users.uuid', '=', 'projects.user_uuid');

            // 1) Сортировка списка проектов
            if (!empty($request->order_by)) {
                $projects->orderByDesc('projects.' . $request->order_by);
            } else {
                $projects->orderByDesc('projects.created_at');
            }

            // 2) Формирование ссылки на фотографию обложки проекта. Kb
            if (!empty($request->image_room_uuid)) {
                $projects->addSelect(DB::raw('(select pi.url from project_images pi
                          where pi.project_uuid = projects.uuid
                            and pi.room_uuid = \'' . $request->image_room_uuid . '\'
                          order by pi.position asc
                          limit 1) as image_url'));
                $projects->addSelect(DB::raw('(select pi.position from project_images pi
                          where pi.project_uuid = projects.uuid
                            and pi.room_uuid = \'' . $request->image_room_uuid . '\'
                          order by pi.position asc
                          limit 1) as image_position'));
                $projects->addSelect(DB::raw('(select pi.sizes_name from project_images pi
                          where pi.project_uuid = projects.uuid
                            and pi.room_uuid = \'' . $request->image_room_uuid . '\'
                          order by pi.position asc
                          limit 1) as image_sizes_name'));
                $projects->addSelect(DB::raw('(select c.hex_code
                          from colors c
                          where c.uuid = (select pic.color_uuid
                                          from project_images_colors pic
                                          where pic.project_image_uuid = (select pi.uuid
                                                                          from project_images pi
                                                                          where pi.project_uuid = projects.uuid
                                                                            and pi.room_uuid = \'' . $request->image_room_uuid . '\'
                                                                          order by pi.position desc
                                                                          limit 1)
                                          order by pic.position asc
                                          limit 1
                          )) as base_color'));
            } else {
                $projects->addSelect(DB::raw('(select pi.url from project_images pi
                          where pi.project_uuid = projects.uuid
                            and pi.position = 0
                          limit 1) as image_url'));
                $projects->addSelect(DB::raw('(select pi.sizes_name from project_images pi
                          where pi.project_uuid = projects.uuid
                            and pi.position = 0
                          limit 1) as image_sizes_name'));
                $projects->addSelect(DB::raw('0 as image_position'));
                $projects->addSelect(DB::raw('(select c.hex_code
                          from colors c
                          where c.uuid = (select pic.color_uuid
                                          from project_images_colors pic
                                          where pic.project_image_uuid = (select pi.uuid
                                                                          from project_images pi
                                                                          where pi.project_uuid = projects.uuid
                                                                          order by pi.position desc
                                                                          limit 1)
                                          order by pic.position asc
                                          limit 1
                          )) as base_color'));
            }


            // 3) Не выводим удаленные проекты
            $projects->whereNull('projects.deleted_at');

            // 4) Не выводим проекты удаленных пользователей
            $projects->whereNull('users.deleted_at');


            // 5) Поиск по конкретному пользователю
            if (!empty($request->user_uuid)) {
                // 5.1) если передан флаг saved_projects
                if ($request->saved_projects != true) {
                    //5.1.1) Выводим принажделаюие пользователью проекты
                    $projects->where('projects.user_uuid', '=', $request->user_uuid);
                } else {
                    //5.1.2) Выводим сохраненные проекты пользователя,
                    $projects->leftJoin('users_saved_projects', 'projects.uuid', '=', 'users_saved_projects.project_uuid');
                    $projects->where('users_saved_projects.user_uuid', '=', $request->user_uuid);
                }

                //5.2) Если авторизированный пользователь является владельцем проекта то выводить и те проекты которые еще на валидации
                if (!$isUserProjectOwner) {
                    $projects->whereNotNull('projects.verified_at');
                }
            } else {
                $projects->whereNotNull('projects.verified_at');
            }


            // 6) Ввод в поле поиска (ищем по названию проекта, по имени пользователя, по тегам)
            //TODO сделать релевантность, сначало выводить те проекты у которых совпало названеи, потом имя пользователя и потом теги
            if (!empty($request->search_text)) {
                $projects->where(function ($query) use ($request) {
                    $search_text = mb_strtolower($request->search_text);

                    // Поиск по названию проекта
                    $query->where(DB::raw('LOWER(projects.name)'), 'like', '%' . $search_text . '%');

                    //Поиск по имени пользователя
                    $query->orWhere(DB::raw('LOWER(users.name)'), 'like', '%' . $search_text . '%');

                    // По тегу, если у проекта есть хотя бы один тег
                    $query->orWhereNotNull(DB::raw("(select pt.name from projects_project_tags
                            left join project_tags pt on projects_project_tags.project_tag_uuid = pt.uuid
                            where projects_project_tags.project_uuid = projects.uuid
                            and pt.name like '%" . $search_text . "%'
                            limit 1)"));
                });
            }


            // 7) Если пользователь авторизован
            if (!empty($request->user())) {
                // 7.1) Пролайкан ли проект
                $projects->addSelect(DB::raw('
                (select
                    (case when pl.project_uuid is not null then true end) as is_liked
                from project_likes pl
                where pl.project_uuid = projects.uuid
                and pl.user_uuid = \'' . $request->user()->uuid . '\'
                limit 1) as is_liked'));

                // 7.2) Сохранен ли проект
                $projects->addSelect(DB::raw('
                (select
                    (case when usp.project_uuid is not null then true end) as is_favorite
                from users_saved_projects usp
                where usp.project_uuid = projects.uuid
                and usp.user_uuid = \'' . $request->user()->uuid . '\'
                limit 1) as is_favorite'));

                // 7.3) Добавлем поле, является ли пользователь владельцем проекта
                $projects->addSelect(DB::raw('(case when projects.user_uuid = \'' . $request->user()->uuid . '\' then true else false end) as user_is_owner'));
            } else {
                $projects->addSelect(DB::raw('0 as is_liked'));
                $projects->addSelect(DB::raw('0 as is_favorite'));
                $projects->addSelect(DB::raw('false as user_is_owner'));
            }


            // Это нужно чтобы отсеивать проекты в которых нет фотографий, когда фильтруем по типу помецщения
            $query = DB::table('projects');
            $query->fromSub($projects, 'res');
            $query->whereNotNull('image_url');

            /////////////
            $result = $query->paginate($request->per_page);

            return response()->json($result);

        } catch (ModelNotFoundException $e) {
            Log::error($e->getMessage(), $e->getIds());

            return response()->json(['success' => false, 'error' => 'Project not found'], 404);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Fail to get project'], 500);
        }


    }

    /**
     * Получаем информацию по одному проекту
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getProject(Request $request)
    {
        $this->validate($request, [
            'project_url_name' => 'required|string',
        ]);

        try {
            $query = Project::query();

            $query->where('url_name', $request->project_url_name);
            $query->with('projectUser', 'projectImages.room', 'projectImages.roomSize', 'projectImages.colors', 'projectTags', 'projectBudget', 'projectStyle');

            // Если пользователь авторизирован
            if (!is_null($request->user())) {
                // Проверяем поставил ли он лайк
                $query->addSelect([
                    'isLiked' => ProjectLike::select('project_uuid')
                        ->whereColumn('project_uuid', 'projects.uuid')
                        ->where('user_uuid', $request->user()->uuid)
                        ->limit(1),
                ]);

                // Проверяем поставил сохранен ли проект
                $query->addSelect([
                    'isFavorite' => UserSavedProject::select('project_uuid')
                        ->whereColumn('project_uuid', 'projects.uuid')
                        ->where('user_uuid', $request->user()->uuid)
                        ->limit(1),
                ]);

                // Проверяем принадлежит ли проект пользователью
                $query->addSelect(DB::raw('(case when projects.user_uuid = \'' . $request->user()->uuid . '\' then true else false end) as user_is_owner'));
            }

            $result = $query->firstOrFail();

            $finalResult                     = new \stdClass();
            $finalResult->projectUuid        = $result->uuid;
            $finalResult->projectName        = $result->name;
            $finalResult->projectDescription = $result->description;
            $finalResult->projectUrl         = $result->url_name;
            $finalResult->projectBudge       = $result->projectBudget;
            $finalResult->projectStyle       = $result->projectStyle;
            $finalResult->likesCounter       = $result->likes_counter;
            $finalResult->viewsCounter       = $result->views_counter;
            $finalResult->updatedAt          = $result->updated_at;
            $finalResult->verifiedAt         = $result->verified_at;
            $finalResult->userName           = $result->projectUser->name;
            $finalResult->userAvatar         = $result->projectUser->avatar_url;
            $finalResult->userUrl            = $result->projectUser->url_name;
            $finalResult->userUrl            = $result->projectUser->url_name;
            $finalResult->userIsOwner        = !is_null($result->user_is_owner) && $result->user_is_owner;
            $finalResult->isLiked            = !is_null($result->isLiked) ? true : null;
            $finalResult->isFavorite         = !is_null($result->isFavorite) ? true : false;

            $tags = [];
            foreach ($result->projectTags as $projectTag) {
                $tags[] = $projectTag->name;
            }
            $finalResult->tags = $tags;

            $images = [];
            foreach ($result->projectImages as $projectImage) {
                $img                   = new \stdClass();
                $img->uuid             = $projectImage->uuid;
                $img->url              = $projectImage->url;
                $img->sizesName       = $projectImage->sizes_name;
                $img->width            = $projectImage->width;
                $img->height           = $projectImage->height;
                $img->position         = $projectImage->position;
                $img->roomUuid         = !empty($projectImage->room_uuid) ? $projectImage->room_uuid : null;
                $img->roomName         = !empty($projectImage->room) ? $projectImage->room->name : null;
                $img->roomNameGenitive = !empty($projectImage->room) ? $projectImage->room->name_genitive : null;
                $img->roomNameSlug     = !empty($projectImage->room) ? $projectImage->room->name_genitive : null;
                $img->roomSizeUuid     = !empty($projectImage->roomSize) ? $projectImage->roomSize->uuid : null;
                $img->roomSizeName     = !empty($projectImage->roomSize) ? $projectImage->roomSize->name : null;
                $img->colors           = [];
                if (!empty($projectImage->colors)) {
                    foreach ($projectImage->colors as $color) {
                        $img->colors[] = $color->hex_code;
                    }
                }

                $images[] = $img;
            }
            $finalResult->images = $images;

            return response()->json($finalResult);

        } catch (ModelNotFoundException $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Project not found'], 404);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Fail to get project'], 500);
        }
    }

    /**
     * @param         $uuid
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function likeProject($uuid, Request $request)
    {
        $request['project_uuid'] = $uuid;

        $this->validate($request, [
            'project_uuid' => 'required|uuid',
        ]);

        try {
            $likeStatus = DB::transaction(function () use ($request) {

                $isExists = ProjectLike::where('project_uuid', '=', $request->project_uuid)
                    ->where('user_uuid', '=', $request->user()->uuid)->exists();

                if ($isExists) {
                    return [
                        'success' => true,
                        'status'  => 'old',
                    ];
                }

                $project               = new ProjectLike();
                $project->project_uuid = $request->project_uuid;
                $project->user_uuid    = $request->user()->uuid;
                $project->setCreatedAt($project->freshTimestamp());
                $project->saveOrFail();

                Project::findOrFail($request->project_uuid)->increment('likes_counter');

                return [
                    'success' => true,
                    'status'  => 'new',
                ];
            });


            return response()->json($likeStatus, 200);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Fail to like project'], 500);
        }
    }

    /**
     * @param         $uuid
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function unlikeProject($uuid, Request $request): \Illuminate\Http\JsonResponse
    {
        $request['project_uuid'] = $uuid;

        $this->validate($request, [
            'project_uuid' => 'required|uuid',
        ]);

        try {
            $unlikeStatus = DB::transaction(function () use ($request) {
                $result = ProjectLike::where('project_uuid', '=', $request->project_uuid)
                    ->where('user_uuid', '=', $request->user()->uuid)
                    ->delete();

                if (!empty($result)) {
                    Project::findOrFail($request->project_uuid)->decrement('likes_counter');

                    return [
                        'success' => true,
                        'status'  => 'new',
                    ];
                }

                return [
                    'success' => true,
                    'status'  => 'old',
                ];
            });


            return response()->json($unlikeStatus, 200);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Fail to unlike project'], 500);
        }
    }

    /**
     * @param         $uuid
     * @param Request $request
     */
    public function addToFavourites($uuid, Request $request)
    {
        $request['project_uuid'] = $uuid;

        $this->validate($request, [
            'project_uuid' => 'required|uuid',
        ]);

        try {
            $saveStatus = DB::transaction(function () use ($request) {
                $isExists = UserSavedProject::where('project_uuid', '=', $request->project_uuid)
                    ->where('user_uuid', '=', $request->user()->uuid)->exists();

                if ($isExists) {
                    return [
                        'success' => true,
                        'status'  => 'old',
                    ];
                }

                $project               = new UserSavedProject();
                $project->user_uuid    = $request->user()->uuid;
                $project->project_uuid = $request->project_uuid;
                $project->setCreatedAt($project->freshTimestamp());
                $project->saveOrFail();

                return [
                    'success' => true,
                    'status'  => 'new',
                ];
            });


            return response()->json($saveStatus, 200);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Fail to save project'], 500);
        }
    }

    /**
     * @param         $uuid
     * @param Request $request
     */
    public function removeFromFavourites($uuid, Request $request)
    {
        $request['project_uuid'] = $uuid;

        $this->validate($request, [
            'project_uuid' => 'required|uuid',
        ]);

        try {

            $result = UserSavedProject::where('project_uuid', '=', $request->project_uuid)
                ->where('user_uuid', '=', $request->user()->uuid)
                ->delete();

            $response = [
                'success' => true,
                'status'  => 'new',
            ];

            if (empty($result)) {
                $response['status'] = 'old';
            }

            return response()->json($response, 200);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Fail to delete project from favorites'], 500);
        }
    }

    /**
     * @param         $uuid
     * @param Request $request
     */
    public function increaseViewsCounter($uuid, Request $request)
    {
        $request['project_uuid'] = $uuid;
        $this->validate($request, [
            'project_uuid' => 'required|uuid',
            'fingerprint'  => 'string|min:2|max:40',
        ]);

        try {
            $cacheKey = 'viewProject:';
            if (!empty($request->user()->uuid)) {
                $cacheKey .= $request->user()->uuid . ':' . $request->project_uuid;
            } elseif (!empty($request->fingerprint)) {
                $cacheKey .= $request->fingerprint . ':' . $request->project_uuid;
            } else {
                throw new Exception('Wrong user identificator');
            }

            if (Redis::get($cacheKey) !== null) {
                return response()->json([
                    'success' => true,
                    'status'  => 'old',
                ], 200);
            }

            $res = Redis::setex($cacheKey, 604800, true);  //7 дней
            if ($res) {
                Project::findOrFail($request->project_uuid)->increment('views_counter');
            }

            return response()->json([
                'success' => true,
                'status'  => 'new',
            ], 200);

        } catch (\Exception $e) {
            Log::error('Fail to increase view project counter:' . $e->getMessage());

            return response()->json(['success' => false, 'error' => 'Fail to increase view project counter'], 500);
        }
    }


}
