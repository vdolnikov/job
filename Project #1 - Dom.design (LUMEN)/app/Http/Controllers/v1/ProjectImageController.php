<?php

namespace App\Http\Controllers\v1;

use App\Models\FileUpload\ImageFileUpload;
use App\Models\FileUpload\ProjectImageFileUpload;
use App\Models\Project;
use App\Models\ProjectImage;
use App\Models\ProjectImageMetaData;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;


class ProjectImageController extends Controller
{

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Throwable
     */
    public function create(Request $request)
    {
        ini_set('max_execution_time', 300);
        $this->validate($request, [
            'project_uuid' => ['required', 'uuid'],
            'newFiles'     => ['required', 'array', "max:10"],
            'meta'         => ['required', 'array'],
            'newFiles.*'   => 'required|mimes:jpeg,png|max:5120',
        ]);

        try {
            // TODO ТУТ СДЕЛАТЬ ОБРАБОТКУ ФОТО И ВАЛИДАЦИЮ C ВОЗВРАЩЕНИЕМ ОШБИОК
            // TODO Сделать проверку на количество фотографий в БД, если повтоно вызывать этот запрос то можно хоть сколько загружать

            if (!$this->isUserProjectOwner($request)) {
                return response()->json(['success' => false, 'error' => 'Access denied for user'], 403);
            }


            $projectImages = new ProjectImage();
            $files         = $request->file('newFiles');

            $filesMetaData = [];
            foreach ($request->meta as $key => $meta) {
                $filesMetaData[$key] = new ProjectImageMetaData($meta);
            }

            $projectImages->addImagesInTransaction($request->project_uuid, $files, ...$filesMetaData);

            return response()->json([
                'success' => true,
            ], 200);


        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Fail to add project images'], 500);
        }
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Throwable
     */
    public function update(Request $request)
    {
        ini_set('max_execution_time', 300);
        $this->validate($request, [
            'project_uuid' => ['required', 'uuid'],
            'oldFiles'     => ['array', "max:10"],
            'newFiles'     => ['array', "max:10"],
            'meta'         => ['required', 'array'],
            'newFiles.*'   => 'mimes:jpeg,png|max:5120',
        ]);


        try {
            if (!$this->isUserProjectOwner($request)) {
                return response()->json(['success' => false, 'error' => 'Access denied for user'], 403);
            }

            DB::transaction(function () use ($request) {
                // 0) Получаем список всех изображений
                $projectImages = DB::table('project_images')->where('project_uuid', $request->project_uuid)->get();

                // 0.1) Фильтруем старые изображения от пользователя
                $oldFiles = [];
                if (isset($request->oldFiles)) {
                    foreach ($request->oldFiles as $key => $oldFile) {
                        $oldFiles[$key] = json_decode($oldFile);
                    }
                }

                // 1) Обновялем информацию о room roomSize если они были измененны
                if (!empty($oldFiles)) {
                    foreach ($projectImages as $projectImage) {
                        foreach ($oldFiles as $key => $oldFile) {
                            if ($projectImage->uuid == $oldFile->uuid) {
                                $isNewData = false;

                                $oldFileRoomUuid = !empty($oldFile->room) ? $oldFile->room->uuid : null;
                                if ($projectImage->room_uuid !== $oldFileRoomUuid) {
                                    $projectImage->room_uuid = $oldFileRoomUuid;
                                    $isNewData               = true;
                                }

                                $oldFileRoomSizeUuid = !empty($oldFile->roomSize) ? $oldFile->roomSize->uuid : null;
                                if ($projectImage->room_size_uuid !== $oldFileRoomSizeUuid) {
                                    $projectImage->room_size_uuid = $oldFileRoomSizeUuid;
                                    $isNewData                    = true;
                                }

                                //1.2) Обновить порядок всех изображений
                                // НУЖНО ОБНОЛЯТЬ Position ТОЛЬКО У СТАРЫХ
                                // У новых изображений при добавлении проставится уже правильное значение position
                                if ($projectImage->position !== $key) {
                                    $projectImage->position = $key;
                                    $isNewData              = true;
                                }


                                if ($isNewData) {
                                    ProjectImage::where("uuid", $projectImage->uuid)->update(
                                        [
                                            "room_uuid"      => $projectImage->room_uuid,
                                            "room_size_uuid" => $projectImage->room_size_uuid,
                                            "position"       => $projectImage->position,
                                        ]
                                    );
                                }
                            }
                        }
                    }
                }

                // 2) Удаляем фотографии из БД и S3, которые были удалены пользователем
                foreach ($projectImages as $projectImage) {
                    $delete = true;
                    foreach ($oldFiles as $oldFile) {
                        if ($projectImage->uuid == $oldFile->uuid) {
                            $delete = false;
                            break;
                        }
                    }

                    if ($delete) {
                        $success = ImageFileUpload::deleteImagesFileFromS3(ProjectImageFileUpload::ROOT_FOLDER, $projectImage->url, explode(',', $projectImage->sizes_name));
                        if (!$success) {
                            throw new Exception('Fail to delete old file in Amazon S3: ' . $projectImage->url . ' | Project_uuid: ' . $request->project_uuid);
                        }

                        ProjectImage::where("uuid", $projectImage->uuid)->delete();
                    }
                }

                // 3) Добавляем новые изображения
                $files = $request->file('newFiles');
                if (isset($files)) {
                    $filesMetaData = [];
                    foreach ($request->meta as $key => $meta) {
                        $filesMetaData[$key] = new ProjectImageMetaData($meta);
                    }
                    (new ProjectImage())->addImages($request->project_uuid, $files, ...$filesMetaData);
                }

            });

            return response()->json([
                'success' => true,
            ], 200);


        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Fail to update project images'], 500);
        }
    }


    /**
     * Проеряем что текущий пользователь является владельцем проекта
     *
     * @param $request
     *
     * @return bool
     */
    private function isUserProjectOwner($request): bool
    {
        $project    = Project::findOrFail($request->project_uuid);
        $user       = new User();
        $user->uuid = $project->user_uuid;

        return $request->user()->can('change-self-data', $user);
    }

}
