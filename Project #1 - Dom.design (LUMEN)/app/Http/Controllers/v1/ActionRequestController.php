<?php

namespace App\Http\Controllers\v1;

use App\Models\ActionRequest;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class ActionRequestController extends Controller
{
    /**
     * Подтверждение email
     *
     * @param         $hash
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function confirmEmail($hash, Request $request)
    {
        $request['hash'] = $hash;
        $this->validate($request, [
            'hash' => 'string',
        ]);

        try {
            $model  = ActionRequest::where('hash', $request->hash)->where('confirmed', 'false');
            $data   = $model->firstOrFail();
            $params = json_decode($data->params);
            $update = $model->update(['confirmed' => true]);

            if (!$update) {
                throw new \Exception('Error update ActionRequest');
            }

            User::where('email', $params->email)->update(['email_verified_at' => Carbon::now()]);

        } catch (ModelNotFoundException $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Not found'], 404);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Fail to confirm email '], 500);
        }

        return response()->json(['success' => true]);
    }

    /**
     * Обновление пароля
     *
     * @param         $hash
     * @param Request $request
     */
    public function resetPassword($hash, Request $request)
    {
        $request['hash'] = $hash;
        $this->validate($request, [
            'hash'                  => 'string',
            'password'              => 'required|min:5',
            'password_confirmation' => 'required|same:password',
        ]);

        try {
            $model  = ActionRequest::where('hash', $request->hash)->where('confirmed', 'false');
            $data   = $model->firstOrFail();
            $params = json_decode($data->params);
            $update = $model->update(['confirmed' => true]);

            if (!$update) {
                throw new \Exception('Error update ActionRequest');
            }

            $user = User::where('email', $params->email)->firstOrFail();
            $user->password = $request->password;
            $user->save();

        } catch (ModelNotFoundException $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Not found'], 404);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Fail to reset password'], 500);
        }

        return response()->json(['success' => true]);
    }
}
