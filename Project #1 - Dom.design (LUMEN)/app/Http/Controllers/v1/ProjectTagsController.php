<?php

namespace App\Http\Controllers\v1;

use App\Models\ProjectTag;
use Illuminate\Http\Request;


class ProjectTagsController extends Controller
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProjectTags(Request $request)
    {
        $this->validate($request, [
            'search' => 'required|min:2|max:100',
            'limit' => 'numeric|min:1|max:30'
        ]);

        if(empty($request->limit)){
            $request->limit = 5;
        }

        $search = mb_strtolower(trim($request->search));
        $search = preg_replace('/\s+/', ' ', $search);

        $result = ProjectTag::where('name', 'like', '%' . $search . '%')
            ->limit($request->limit)
            ->get();

        return response()->json($result);
    }

}
