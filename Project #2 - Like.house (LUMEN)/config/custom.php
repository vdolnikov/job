<?php

return [

    'frontend_url' => env('FRONTEND_URL'),

    'google_captcha_secret'          => env('GOOGLE_CAPTCHA_SECRET'),
    'google_captcha_unittest_secret' => env('GOOGLE_CAPTCHA_UNITTEST_SECRET'),

    'wiq_api_link' => env('WIQ_API_LINK'),
    'wiq_api_key'  => env('WIQ_API_KEY'),


    'rabbit_host' => env('RABBIT_HOST'),
    'rabbit_port' => env('RABBIT_PORT'),
    'rabbit_user' => env('RABBIT_USER'),
    'rabbit_pass' => env('RABBIT_PASS'),


    'yookassa_return_link' => env('YOOKASSA_RETURN_LINK'),
    'yookassa_login'       => env('YOOKASSA_LOGIN'),
    'yookassa_password'    => env('YOOKASSA_PASSWORD'),

];
