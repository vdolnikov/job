<?php

return [

    /*
   |--------------------------------------------------------------------------
   | Параметры инвалижации кеша
   |--------------------------------------------------------------------------
   |
   | В нужном методе перечислить ключи кеша, при вызове этоко метода
   | кеш удалится по ключам
   |
   */
    'invalidateCache' => [
        'GET'    => [
            'UserController@getUserList'                       => [],
            'UserController@getUserInfoByUUID'                 => [],
            'AuthController@logout'                            => [],
            'AuthController@profile'                           => [],
        ],
        'POST'   => [
            'UserController@create'                   => [],
            'AuthController@login'                    => [],
            'AuthController@refresh'                  => [],
            'ActionRequestController@confirmEmail'    => [],
            'ActionRequestController@resetPassword'   => [],
            'EmailController@sendMailToResetPassword' => [],
        ],
        'DELETE' => [

        ],
        'PATCH'  => [
            'UserController@updateByUUID'            => [],
            'UserController@updatePasswordByUUID'    => [],
        ],
    ],
];



