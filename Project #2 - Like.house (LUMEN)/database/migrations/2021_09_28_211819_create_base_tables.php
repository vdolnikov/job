<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBaseTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Таблица всех соц сетей
         */
        if (!Schema::hasTable('social_networks')) {
            Schema::create('social_networks', function (Blueprint $table) {
                $table->uuid('uuid')->primary();
                $table->string('name', 50);
                $table->string('slug', 50);
                $table->string('url', 100);
                $table->string('logo', 30)->nullable();
                $table->string('logoColor', 7)->default('#000000');
                $table->integer('position')->default('9999');
                $table->boolean('disabled')->default(true);
                $table->softDeletes();
                $table->index(['name', 'slug']);
            });
        }

        /**
         * Таблица для событиев
         */
        if (!Schema::hasTable('action_request')) {
            Schema::create('action_request', function (Blueprint $table) {
                $table->uuid('uuid')->primary();
                $table->string('action', 50);
                $table->string('hash', 50)->unique();
                $table->jsonb('params');
                $table->boolean('confirmed')->default(false);
                $table->timestamps();
                $table->index(['hash']);
            });
        }

        /**
         * Таблица поставщиков услуг
         */
        if (!Schema::hasTable('providers')) {
            Schema::create('providers', function (Blueprint $table) {
                $table->uuid('uuid')->primary();
                $table->string('name', 50);
                $table->string('slug', 50);
                $table->string('class_name', 50);
                $table->softDeletes();
                $table->index(['name', 'slug']);
            });
        }

        /**
         * Таблица (услуг, подписка, лайк и тд)
         */
        if (!Schema::hasTable('services')) {
            Schema::create('services', function (Blueprint $table) {
                $table->uuid('uuid')->primary();
                $table->uuid('social_network_uuid');
                $table->string('name', 50);
                $table->string('slug', 50);
                $table->integer('position')->default('9999');
                $table->softDeletes();
                $table->foreign('social_network_uuid')->references('uuid')->on('social_networks')->onDelete('cascade')->onUpdate('cascade');
                $table->index(['name', 'slug', 'social_network_uuid']);
            });
        }

        /**
         * Таблица услуг/продуктов у поставщиков
         */
        if (!Schema::hasTable('provider_products')) {
            Schema::create('provider_products', function (Blueprint $table) {
                $table->uuid('uuid')->primary();
                $table->uuid('provider_uuid');
                $table->uuid('service_uuid');
                $table->integer('provider_product_id');
                $table->string('key', 100);
                $table->string('name', 100);
                $table->decimal('cost', 10, 4);
                $table->text('description')->nullable();
                $table->timestamps();
                $table->foreign('provider_uuid')->references('uuid')->on('providers')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('service_uuid')->references('uuid')->on('services')->onDelete('cascade')->onUpdate('cascade');
                $table->index(['key', 'provider_product_id']);
            });
        }

        /**
         * Таблица продуктов сервиса. Все услуги
         */
        if (!Schema::hasTable('products')) {
            Schema::create('products', function (Blueprint $table) {
                $table->uuid('uuid')->primary();
                $table->uuid('service_uuid');
                $table->uuid('provider_product_uuid')->nullable();
                $table->string('name', 100);
                $table->string('key', 100);
                $table->string('product_link', 100)->nullable();
                $table->decimal('cost', 10, 4);
                $table->text('description')->nullable();
                $table->string('speed', 30)->nullable();
                $table->string('quality', 30)->nullable();
                $table->string('guarantee', 30)->nullable();
                $table->string('bounce_rate', 30)->nullable();
                $table->string('launch_type', 30)->nullable();
                $table->integer('min_order');
                $table->integer('max_order');
                $table->timestamps();
                $table->softDeletes();
                $table->foreign('service_uuid')->references('uuid')->on('services')->onDelete('cascade')->onUpdate('cascade');
                $table->foreign('provider_product_uuid')->references('uuid')->on('provider_products')->onDelete('set null');
                $table->index(['key', 'created_at']);
            });
        }

        if (!Schema::hasTable('service_types')) {
            Schema::create('service_types', function (Blueprint $table) {
                $table->uuid('uuid')->primary();
                $table->uuid('service_uuid');
                $table->string('name', 100);
                $table->string('key', 30);
                $table->boolean('checked')->default(false);
                $table->integer('position');
                $table->foreign('service_uuid')->references('uuid')->on('services')->onDelete('cascade')->onUpdate('cascade');
                $table->index(['service_uuid']);
            });
        }

        /**
         * Создаем таблицу пользователей
         */
        if (!Schema::hasTable('users')) {
            Schema::create('users', function (Blueprint $table) {
                $table->uuid('uuid')->primary();
                $table->string('email', 100)->unique();
                $table->string('password', 100);
                $table->decimal('balance', 10, 4);
                $table->boolean('is_admin')->default(false);
                $table->timestamp('email_verified_at')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }

        /**
         * Таблица заказов
         */
        if (!Schema::hasTable('orders')) {
            Schema::create('orders', function (Blueprint $table) {
                $table->uuid('uuid')->primary();
                $table->integer('id');
                $table->integer('provider_id')->nullable();
                $table->uuid('user_uuid')->nullable();
                $table->uuid('product_uuid')->nullable();
                $table->string('order_status', 20);
                $table->string('link', 150);
                $table->integer('count');
                $table->decimal('total_cost', 10, 4);
                $table->decimal('expenses', 10, 4);
                $table->integer('start_count')->default(0)->nullable();
                $table->integer('remains')->default(0)->nullable();
                $table->timestamps();
                $table->softDeletes();
                $table->foreign('user_uuid')->references('uuid')->on('users')->onDelete('set null')->onUpdate('cascade');
                $table->foreign('product_uuid')->references('uuid')->on('products')->onDelete('set null')->onUpdate('cascade');
                $table->index(['id', 'link', 'user_uuid', 'created_at']);
            });
        }

        /**
         * Таблица пополнения баланса
         */
        if (!Schema::hasTable('payment_history')) {
            Schema::create('payment_history', function (Blueprint $table) {
                $table->uuid('uuid')->primary();
                $table->uuid('user_uuid')->nullable();
                $table->uuid('payment_id');
                $table->string('payment_service_name');
                $table->decimal('amount', 10, 4);
                $table->string('currency', '5')->default("RUB");
                $table->string('status', '20');
                $table->timestamps();
                $table->softDeletes();
                $table->foreign('user_uuid')->references('uuid')->on('users')->onDelete('set null')->onUpdate('cascade');
                $table->index(['user_uuid', 'payment_id', 'status', 'created_at']);
            });
        }

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_history');
        Schema::dropIfExists('orders');
        Schema::dropIfExists('users');
        Schema::dropIfExists('service_types');
        Schema::dropIfExists('products');
        Schema::dropIfExists('provider_products');
        Schema::dropIfExists('services');
        Schema::dropIfExists('providers');
        Schema::dropIfExists('action_request');
        Schema::dropIfExists('social_networks');
    }
}
