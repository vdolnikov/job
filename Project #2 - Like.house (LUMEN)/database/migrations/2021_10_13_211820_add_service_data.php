<?php

use App\Models\Product;
use App\Models\Provider;
use App\Models\ProviderProduct;
use App\Models\Service;
use App\Models\ServiceType;
use App\Models\SocialNetwork;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;


class AddServiceData extends Migration
{

    public function up()
    {
        $this->addUsers();
        $this->addSocialNetwork();
        $this->appProvider();
        $this->addServices();
        $this->addServiceTypes();
        $this->addProviderProducts();
        $this->addProducts();
    }


    public function down()
    {

    }

    public function appProvider()
    {
        $p1             = new Provider();
        $p1->name       = "Wiq";
        $p1->slug       = "wiq";
        $p1->class_name = "WiqProvider";
        $p1->save();
    }

    public function addUsers()
    {
        $user1                    = new User();
        $user1->email             = 'v.dolnikov@yandex.ru';
        $user1->password          = '123123';
        $user1->balance           = 100.0000;
        $user1->is_admin          = true;
        $user1->email_verified_at = '2021-10-05 08:56:12';
        $user1->created_at        = '2021-10-05 08:56:03';
        $user1->updated_at        = '2021-10-05 08:56:12';
        $user1->save();
    }

    public function addSocialNetwork()
    {
        $s1            = new SocialNetwork();
        $s1->name      = "Instagram";
        $s1->slug      = "instagram";
        $s1->url       = "https://www.instagram.com/";
        $s1->logo      = "instagram";
        $s1->logoColor = "#833AB4";
        $s1->disabled  = false;
        $s1->position  = 1;
        $s1->save();

        $s2            = new SocialNetwork();
        $s2->name      = "YouTube";
        $s2->slug      = "youtube";
        $s2->url       = "https://www.youtube.com/";
        $s2->logo      = "youtube";
        $s2->logoColor = "#C4302B";
        $s2->disabled  = false;
        $s2->position  = 2;
        $s2->save();

        $s2            = new SocialNetwork();
        $s2->name      = "Вконтакте";
        $s2->slug      = "vk";
        $s2->url       = "https://vk.com/";
        $s2->logo      = "vk";
        $s2->logoColor = "#2787F5";
        $s2->disabled  = false;
        $s2->position  = 3;
        $s2->save();

        $s2            = new SocialNetwork();
        $s2->name      = "TikTok";
        $s2->slug      = "tiktok";
        $s2->url       = "https://www.tiktok.com/";
        $s2->logo      = "tiktok";
        $s2->logoColor = "#000000";
        $s2->disabled  = false;
        $s2->position  = 4;
        $s2->save();

        $s2            = new SocialNetwork();
        $s2->name      = "Telegram";
        $s2->slug      = "telegram";
        $s2->url       = "https://telegram.org/";
        $s2->logo      = "telegram";
        $s2->logoColor = "#0088CC";
        $s2->disabled  = false;
        $s2->position  = 5;
        $s2->save();

        $s2            = new SocialNetwork();
        $s2->name      = "Twitter";
        $s2->slug      = "twitter";
        $s2->url       = "https://twitter.com/";
        $s2->logo      = "twitter";
        $s2->logoColor = "#1DA1F2";
        $s2->disabled  = true;
        $s2->position  = 6;
        $s2->save();


        $s2            = new SocialNetwork();
        $s2->name      = "Facebook";
        $s2->slug      = "facebook";
        $s2->url       = "https://facebook.com/";
        $s2->logo      = "facebook";
        $s2->logoColor = "#3B5998";
        $s2->disabled  = true;
        $s2->position  = 7;
        $s2->save();

        $s2            = new SocialNetwork();
        $s2->name      = "Одноклассники";
        $s2->slug      = "ok";
        $s2->url       = "https://ok.ru/";
        $s2->logo      = "odnoklassniki";
        $s2->logoColor = "#ED812B";
        $s2->disabled  = true;
        $s2->position  = 8;
        $s2->save();

    }

    public function addServices()
    {
        $nw_instagram = SocialNetwork::where('slug', 'instagram')->first();

        $p1                      = new Service();
        $p1->social_network_uuid = $nw_instagram->uuid;
        $p1->name                = 'Подписчики';
        $p1->slug                = 'followers';
        $p1->position            = 1;
        $p1->save();

        $p2                      = new Service();
        $p2->social_network_uuid = $nw_instagram->uuid;
        $p2->name                = 'Лайки';
        $p2->slug                = 'likes';
        $p2->position            = 2;
        $p2->save();

        $p3                      = new Service();
        $p3->social_network_uuid = $nw_instagram->uuid;
        $p3->name                = 'Авто лайки/просмотры';
        $p3->slug                = 'auto-likes';
        $p3->position            = 3;
        $p3->save();

        $p4                      = new Service();
        $p4->social_network_uuid = $nw_instagram->uuid;
        $p4->name                = 'Просмотры постов';
        $p4->slug                = 'post-views';
        $p4->position            = 4;
        $p4->save();

        $p5                      = new Service();
        $p5->social_network_uuid = $nw_instagram->uuid;
        $p5->name                = 'Просмотры историй';
        $p5->slug                = 'story-views';
        $p5->position            = 5;
        $p5->save();

        $p6                      = new Service();
        $p6->social_network_uuid = $nw_instagram->uuid;
        $p6->name                = 'Комментарии';
        $p6->slug                = 'comments';
        $p6->position            = 6;
        $p6->save();

        $p7                      = new Service();
        $p7->social_network_uuid = $nw_instagram->uuid;
        $p7->name                = 'Статистика';
        $p7->slug                = 'statistics';
        $p7->position            = 7;
        $p7->save();


        //TIK TOK
        $nw_tiktok = SocialNetwork::where('slug', 'tiktok')->first();

        $p8                      = new Service();
        $p8->social_network_uuid = $nw_tiktok->uuid;
        $p8->name                = 'Подписчики';
        $p8->slug                = 'followers';
        $p8->position            = 1;
        $p8->save();

        $p9                      = new Service();
        $p9->social_network_uuid = $nw_tiktok->uuid;
        $p9->name                = 'Лайки';
        $p9->slug                = 'likes';
        $p9->position            = 2;
        $p9->save();

        $p10                      = new Service();
        $p10->social_network_uuid = $nw_tiktok->uuid;
        $p10->name                = 'Просмотры';
        $p10->slug                = 'views';
        $p10->position             = 3;
        $p10->save();

        $p11                      = new Service();
        $p11->social_network_uuid = $nw_tiktok->uuid;
        $p11->name                = 'Комментарии';
        $p11->slug                = 'comments';
        $p11->position             = 4;
        $p11->save();

        //VK
        $nw_vk                    = SocialNetwork::where('slug', 'vk')->first();
        $p12                      = new Service();
        $p12->social_network_uuid = $nw_vk->uuid;
        $p12->name                = 'Подписчики';
        $p12->slug                = 'followers';
        $p12->position             = 1;
        $p12->save();

        $p13                      = new Service();
        $p13->social_network_uuid = $nw_vk->uuid;
        $p13->name                = 'Лайки';
        $p13->slug                = 'likes';
        $p13->position             = 2;
        $p13->save();

        $p14                      = new Service();
        $p14->social_network_uuid = $nw_vk->uuid;
        $p14->name                = 'Репосты';
        $p14->slug                = 'reposts';
        $p14->position             = 3;
        $p14->save();

        $p15                      = new Service();
        $p15->social_network_uuid = $nw_vk->uuid;
        $p15->name                = 'Просмотры';
        $p15->slug                = 'views';
        $p15->position             = 4;
        $p15->save();

        //YouTube
        $nw_youtube               = SocialNetwork::where('slug', 'youtube')->first();
        $p16                      = new Service();
        $p16->social_network_uuid = $nw_youtube->uuid;
        $p16->name                = 'Лайки';
        $p16->slug                = 'likes';
        $p16->position             = 1;
        $p16->save();

        $p17                      = new Service();
        $p17->social_network_uuid = $nw_youtube->uuid;
        $p17->name                = 'Просмотры';
        $p17->slug                = 'views';
        $p17->position             = 2;
        $p17->save();

        //Telegram
        $nw_telegram              = SocialNetwork::where('slug', 'telegram')->first();
        $p18                      = new Service();
        $p18->social_network_uuid = $nw_telegram->uuid;
        $p18->name                = 'Подписчики';
        $p18->slug                = 'followers';
        $p18->position             = 1;
        $p18->save();

        $p19                      = new Service();
        $p19->social_network_uuid = $nw_telegram->uuid;
        $p19->name                = 'Просмотры';
        $p19->slug                = 'views';
        $p19->position             = 2;
        $p19->save();
    }

    public function addServiceTypes()
    {
        $nw_instagram = SocialNetwork::where('slug', 'instagram')->first();

        $s = Service::where('slug', 'followers')->where('social_network_uuid', $nw_instagram->uuid)->first();

        $st1               = new ServiceType();
        $st1->service_uuid = $s->uuid;
        $st1->name         = 'Роботы';
        $st1->key          = 'BOTS';
        $st1->position     = 1;
        $st1->checked      = false;
        $st1->save();
    }


    public function addProviderProducts()
    {
        $provider                = Provider::where('slug', 'wiq')->first();
        $sn_instagram            = SocialNetwork::where('slug', 'instagram')->first();
        $service1                = Service::where('social_network_uuid', $sn_instagram->uuid)->where('slug', 'followers')->first();
        $p1                      = new ProviderProduct();
        $p1->provider_uuid       = $provider->uuid;
        $p1->service_uuid        = $service1->uuid;
        $p1->provider_product_id = '10';
        $p1->key                 = 'WIQ_INSTAGRAM_FOLLOWERS_BOTS';
        $p1->name                = 'Подписчики MIX (ГАРАНТИЯ 1 ГОД) (️R365)';
        $p1->cost                = '0.051';
        $p1->description         = '- Все профили с аватарками и публикациями! Присутствуют реальные пользователи.
- Примерно 20-30% Российских пользователей.
- Без отписок
- Гарантия от списаний - 365 дней.
- Можно заказывать повторно после выполнения.';
        $p1->save();

        $p2                      = new ProviderProduct();
        $p2->provider_uuid       = $provider->uuid;
        $p2->service_uuid        = $service1->uuid;
        $p2->provider_product_id = '62';
        $p2->key                 = 'WIQ_INSTAGRAM_FOLLOWERS_REAL';
        $p2->name                = 'Подписчики RU РЕАЛЬНЫЕ VIP (Без отписок) (R365)';
        $p2->cost                = '0.199';
        $p2->description         = '- Подписчики - РЕАЛЬНЫЕ ПОЛЬЗОВАТЕЛИ.
- Реальные люди выполняют задания на подписку к Вам с биржи заданий.
- Естественная раскрутка.
- Гарантия от отписок 365 дней.
- Вместе с подписками увеличиваются посещения профиля.
- Накрутка производится с запасом.
- 90% подписчиков из России и СНГ.
- Подписчики живые! Могут проявлять активность в Вашем профиле - ставить лайки, смотреть истории, оставлять комментарии.';
        $p2->save();

        $p2                      = new ProviderProduct();
        $p2->provider_uuid       = $provider->uuid;
        $p2->service_uuid        = $service1->uuid;
        $p2->provider_product_id = '10';
        $p2->key                 = 'WIQ_INSTAGRAM_FOLLOWERS_SPEED';
        $p2->name                = 'Подписчики MIX + REAL (♻️ R30)';
        $p2->cost                = '0.039';
        $p2->description         = '- Подписчики - РЕАЛЬНЫЕ ПОЛЬЗОВАТЕЛИ';
        $p2->save();
    }


    private function addProducts()
    {
        $sn_instagram      = SocialNetwork::where('slug', 'instagram')->first();
        $service_followers = Service::where('social_network_uuid', $sn_instagram->uuid)->where('slug', 'followers')->first();
        $pp_bots           = ProviderProduct::where('key', 'WIQ_INSTAGRAM_FOLLOWERS_BOTS')->first();
        $pp_real           = ProviderProduct::where('key', 'WIQ_INSTAGRAM_FOLLOWERS_REAL')->first();
        $pp_speed          = ProviderProduct::where('key', 'WIQ_INSTAGRAM_FOLLOWERS_REAL')->first();

        $p1                        = new Product();
        $p1->service_uuid          = $service_followers->uuid;
        $p1->provider_product_uuid = $pp_bots->uuid;
        $p1->key                   = 'INSTAGRAM_FOLLOWERS_BOTS';
        $p1->name                  = 'Подписчики в Instagram (Роботы)';
        $p1->product_link          = '/instagram/followers';
        $p1->cost                  = '0.35';
        $p1->description           = '- Все профили с аватарками и публикациями! Присутствуют реальные пользователи.
- Примерно 20-30% Российских пользователей.
- Без отписок
- Гарантия от списаний - 365 дней.
- Можно заказывать повторно после выполнения.';
        $p1->speed                 = 'до 500';
        $p1->quality               = 'Высокое';
        $p1->guarantee             = 'Услуга с гарантией';
        $p1->bounce_rate           = '1%';
        $p1->launch_type           = 'Мнговенно';
        $p1->min_order             = '10';
        $p1->max_order             = '20000';
        $p1->save();


        $p2                        = new Product();
        $p2->service_uuid          = $service_followers->uuid;
        $p2->provider_product_uuid = $pp_real->uuid;
        $p2->key                   = 'INSTAGRAM_FOLLOWERS_REAL';
        $p2->name                  = 'Подписчики в Instagram (Живые)';
        $p2->product_link          = '/instagram/followers';
        $p2->cost                  = '1.00';
        $p2->description           = "- Подписчики - РЕАЛЬНЫЕ ПОЛЬЗОВАТЕЛИ.
- Реальные люди выполняют задания на подписку к Вам с биржи заданий.
- Естественная раскрутка.
- Гарантия от отписок 365 дней.
- Вместе с подписками увеличиваются посещения профиля.
- Накрутка производится с запасом.
- 90% подписчиков из России и СНГ.
- Подписчики живые! Могут проявлять активность в Вашем профиле - ставить лайки, смотреть истории, оставлять комментарии.";
        $p2->speed                 = '146';
        $p2->quality               = 'Высокое';
        $p2->guarantee             = 'Услуга с гарантией';
        $p2->bounce_rate           = '2%';
        $p2->launch_type           = 'В течение 5 минут';
        $p2->min_order             = '5';
        $p2->max_order             = '20000';
        $p2->save();


        $p3                        = new Product();
        $p3->service_uuid          = $service_followers->uuid;
        $p3->provider_product_uuid = $pp_real->uuid;
        $p3->key                   = 'INSTAGRAM_FOLLOWERS_SPEED';
        $p3->name                  = 'Подписчики в Instagram (Ускоренные)';
        $p3->product_link          = '/instagram/followers';
        $p3->cost                  = '0.16';
        $p3->description           = '
        Для обеспечения максимальной скорости на Ваш аккаунт будут подписываться подписчики из разных стран.<br>
         При этом качество подписчиков будет высокое (все профили с аватарками и публикациями).<br>
         Из за высокой скорости инстаграм может частично списывать подписчиков, поэтому мы добавим вам немного больше подписчиков! ';
        $p3->speed                 = 'до 10000';
        $p3->quality               = 'Высокое';
        $p3->guarantee             = 'Услуга с гарантией';
        $p3->bounce_rate           = '10%';
        $p3->launch_type           = 'В течение 5 минут';
        $p3->min_order             = '500';
        $p3->max_order             = '200000';
        $p3->save();
    }
}
