<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;


class FastOrderChanges extends Migration
{
    public function up()
    {
        Schema::table('orders', function($table) {
            $table->string('payment_link', 200)->nullable();
        });

        Schema::table('payment_history', function($table) {
            $table->string('callback', 250)->nullable();
        });
    }


    public function down()
    {
        Schema::table('orders', function($table) {
            $table->dropColumn('payment_link');
        });

        Schema::table('payment_history', function($table) {
            $table->dropColumn('callback');
        });
    }
}
