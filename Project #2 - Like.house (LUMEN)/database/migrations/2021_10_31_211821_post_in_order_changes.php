<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;


class PostInOrderChanges extends Migration
{
    public function up()
    {
        Schema::table('orders', function($table) {
            $table->integer('posts_count')->nullable();
        });
    }


    public function down()
    {
        Schema::table('orders', function($table) {
            $table->dropColumn('posts_count');
        });
    }
}
