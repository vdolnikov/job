<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Подтверждение Email</title>
    <style>
        *,
        *::before,
        *::after {
            box-sizing: border-box;
        }

        html {
            font-family: sans-serif;
            line-height: 1.15;
            -webkit-text-size-adjust: 100%;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
        }

        article, aside, figcaption, figure, footer, header, hgroup, main, nav, section {
            display: block;
        }

        body {
            margin: 0;
            font-family: sans-serif;
            font-weight: 400;
            color: #0d0906;
            text-align: left;
            background-color: #f5f5f5;
            font-size: 16px;
            line-height: 24px;
        }

        [tabindex="-1"]:focus:not(:focus-visible) {
            outline: 0 !important;
        }


        a, a:hover {
            -webkit-transition: all 0.2s ease-in-out;
            -moz-transition: all 0.2s ease-in-out;
            -o-transition: all 0.2s ease-in-out;
            -ms-transition: all 0.2s ease-in-out;
            transition: all 0.2s ease-in-out;
        }

        .container-main {
            padding: 0 44px;
            margin: 0 auto;
        }


        .mail-block {
            background: #ffffff;
            min-height: 200px;
            border-radius: 20px;
            box-shadow: 0 6px 10px 10px rgba(60, 28, 18, 0.03);
            padding: 50px;
            margin: 30px 0 0 0;
        }

        .mail-block__title {
            font-size: 32px;
            line-height: 40px;
            font-weight: 600;
        }

        .mail-block__text {
            margin-top: 20px;
            font-size: 18px;
        }

        .mail-block__subtext {
            margin-top: 5px;
            color: #867f7b;
        }

        .mail-block__button {
            display: inline-block;
            margin-top: 20px;
            min-width: 20%;
            min-height: 45px;
            background: #FF7A4F;
            border-radius: 14px;
            color: #fff;
            border: none;
            padding: 10px 20px 10px 20px;
            font-weight: 400;
            font-size: 16px;
            line-height: 24px;
            text-align: center;
            -webkit-transition: all 0.2s ease-in-out;
            -moz-transition: all 0.2s ease-in-out;
            -o-transition: all 0.2s ease-in-out;
            -ms-transition: all 0.2s ease-in-out;
            transition: all 0.2s ease-in-out;
            cursor: pointer !important;
            text-decoration: none;
        }

        .mail-block__button:hover {
            background-color: #E7663D;
        }

        .mail-block__text-link{
            display: block;
            margin: 20px 0 20px 0;
        }

    </style>
</head>
<body>
<div class="container-main">
    <br>
    <div class="mail-block">
        <div class="mail-block__title">Мы получили запрос на сброс вашего пароля в <a href="https://likes.house" style="color: #FF7A4F">Likes.House</a></div>
        <div class="mail-block__text">Если вы проигнорируете это сообщение, ваш пароль не будет изменен.<br>Если вы не запрашивали сброс пароля, сообщите нам об этом на: <b>support@likes.house</b></div><br>

        <div class="mail-block__subtext">Для сброса пароля нажмите на кнопку ниже:</div>
        <a href="<?= config('custom.frontend_url') ?>/auth/reset-password?hash=<?= $data['hash'] ?>" class="mail-block__button">Сбросить пароль</a>
        <a href="<?= config('custom.frontend_url') ?>/auth/reset-password?hash=<?= $data['hash'] ?>" class="mail-block__text-link"><?= config('custom.frontend_url') ?>/auth/reset-password?hash=<?= $data['hash'] ?></a>
    </div>
    <br>
    <br>
</div>
</body>
</html>
