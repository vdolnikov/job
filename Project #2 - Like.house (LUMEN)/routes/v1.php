<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/*******************
 * User Controller
 ******************/
$router->get('/users', 'UserController@getUserList');
$router->get('/users/{uuid}', 'UserController@getUserInfoByUUID');
$router->post('/users', 'UserController@create');
$router->patch('/users/{uuid}/password/change', ['middleware' => ['auth'], 'uses' => 'UserController@updatePasswordByUUID']);

/*******************
 * Auth Controller
 ******************/
$router->post('/login', 'AuthController@login');
$router->post('/refresh', 'AuthController@refresh');
$router->get('/logout', 'AuthController@logout');
$router->get('/profile', ['middleware' => ['auth'], 'uses' => 'AuthController@profile']);

/*******************
 * Action Request Controller
 ******************/
$router->post('/action/email/confirm/{hash}', 'ActionRequestController@confirmEmail');
$router->post('/action/password/reset/{hash}', 'ActionRequestController@resetPassword');


/*******************
 * Email Controller
 ******************/
$router->post('/email/password/reset', 'EmailController@sendMailToResetPassword');
$router->post('/email/verification/resend', ['middleware' => ['auth'], 'uses' => 'EmailController@resendMailToVerificationEmail']);

/*******************
 * Parser Controller
 ******************/
$router->get('/parser/instagram/subscribers', 'ParserController@instagramSubscribers');

/*******************
 * Product Controller
 ******************/
$router->get('/product', 'ProductController@getProductInfo');

/*******************
 * Services Controller
 ******************/
$router->get('/services', 'ServiceController@getServices');

/*******************
 * Order Controller
 ******************/
$router->get('/order', ['middleware' => ['auth'], 'uses' => 'OrderController@getUserOrders']);
$router->post('/order', ['middleware' => ['auth', 'emailConfirmed'], 'uses' => 'OrderController@create']);
$router->post('/order/fast',  'OrderController@createFastOrder');
$router->post('/order/status/change', 'OrderController@changeOrderStatus');
$router->patch('/order', ['middleware' => ['auth'], 'uses' => 'OrderController@cancelOrder']);


/*******************
 * Payment Controller
 ******************/
$router->get('/payment', ['middleware' => ['auth'], 'uses' => 'PaymentController@create']);
$router->post('/payment/change/status', 'PaymentController@setPaymentStatus');


