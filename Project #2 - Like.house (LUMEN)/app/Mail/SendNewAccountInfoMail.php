<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


class SendNewAccountInfoMail extends Mailable
{
    use Queueable, SerializesModels;

    public $email;

    public $password;

    public function __construct($email, $password)
    {
        $this->email = $email;
        $this->password = $password;
    }

    public function build()
    {
        $subject = 'Данный для авторизации на Likes.House';
        return $this->view('emails.NewAccountInfo')
            ->subject($subject)
            ->with([
                'email' => $this->email,
                'password' => $this->password,
            ]);
    }
}
