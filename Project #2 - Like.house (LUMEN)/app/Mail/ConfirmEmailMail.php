<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


class ConfirmEmailMail extends Mailable
{
    use Queueable, SerializesModels;

    public $hash;

    public function __construct($hash)
    {
        $this->hash = $hash;
    }

    public function build()
    {
        $subject = 'Подтвердите аккаунт на Likes.House';

        return $this->view('emails.ConfirmEmail')
            ->subject($subject)
            ->with([
                'data' => $this->hash,
            ]);
    }
}
