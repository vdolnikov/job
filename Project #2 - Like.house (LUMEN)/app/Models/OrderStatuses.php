<?php

namespace App\Models;

class OrderStatuses
{
    const STATUS_PENDING    = 'Pending';
    const STATUS_PROCESSING = 'Processing';
    const STATUS_IN_PROCESS = 'In progress';
    const STATUS_COMPLETED  = 'Completed';
    const STATUS_ERROR      = 'Error';
    const STATUS_CANCELED   = 'Canceled';
    const STATUS_PARTIAL    = 'Partial';
    const AWAITING_PAYMENT  = 'Awaiting payment';


    static function getFinalStatuses(): array
    {
        return [
            self::STATUS_COMPLETED,
            self::STATUS_CANCELED,
            self::STATUS_ERROR,
        ];
    }
}
