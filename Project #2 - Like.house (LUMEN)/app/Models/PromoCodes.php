<?php

namespace App\Models;


class PromoCodes
{
    const promoCodes = [
        'WOW' => 0.5,
    ];

    static function getDiscountPercentage($promoCode)
    {
        return self::promoCodes[$promoCode];
    }
}
