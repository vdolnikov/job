<?php

namespace App\Models;

use App\Mail\ConfirmEmailMail;
use App\Mail\SendNewAccountInfoMail;
use App\Models\ProductProviders\ProviderMethods\CreateOrder;
use App\Models\ProductProviders\ServiceProviderInterface;
use App\Models\ProductProviders\WiqProvider;
use App\Queue\WiqStatusQueue;
use App\Traits\PrimaryKeyUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Exceptions\PayException;
use YooKassa\Model\NotificationEventType;

class Order extends Model
{
    use PrimaryKeyUuid;

    protected $table = "orders";


    public function product()
    {
        return $this->belongsTo(Product::class, 'product_uuid');
    }

    /**
     * Заупскаем создание заказа
     *
     * @param Request $request
     *
     * Параметр amountPosts для услуги авто лайки/просмотры, там указывается количество постов
     *
     * @return bool
     * @throws \Throwable
     * @throws PayException
     */
    public function createOrder(Request $request): bool
    {
        // 1) Подгружаем нужные данные из БД
        $authUser = JWTAuth::user();
        $product  = Product::where('key', $request->key)->with('providerProduct.provider')->firstOrFail();

        // 2) Проверяем что сумма с фронта и с бека одинаковая
        $totalCost = round($product->cost * $request->amount, 3);
        $expenses  = round($product->providerProduct->cost * $request->amount, 3);
        if (isset($request->amountPosts)) {
            $totalCost = $totalCost * $request->amountPosts;
            $expenses  = $expenses * $request->amountPosts;
        }

        if ((float)$request->totalCost != $totalCost) {
            throw new \Exception('Total cost incorrect: ' . $totalCost . ' - ' . $request->totalCost);
        }

        // 3) Списываем деньги со счета
        User::where('uuid', $authUser->uuid)->firstOrFail()->pay($totalCost);

        // 4) Запускаем выполнение задачи к провайдера
        //$order_id = mt_rand(100000000, 999999999);
        $order_id = $this->creatingOrderWithProvider(
            $product,
            $request->amount,
            $request->link,
            $request->amountPosts ?? null,
        )->provider_order_id;
        if (is_int($order_id)) {
            throw new \Exception('Something wrong! $order_id in incorrect. $order_id = ' . $order_id);
        }

        // 5) Сохранение данных в БД
        $count = $request->amount;
        $remains = $count;
        if (isset($request->amountPosts)) {
            $remains = $request->amountPosts;
            $this->posts_count = $request->amountPosts;
        }

        $this->id           = mt_rand(100000000, 999999999);
        $this->provider_id  = $order_id;
        $this->user_uuid    = $authUser->uuid;
        $this->product_uuid = $product->uuid;
        $this->order_status = OrderStatuses::STATUS_PENDING;
        $this->link         = $request->link;
        $this->count        = $count;
        $this->total_cost   = $totalCost;
        $this->expenses     = $expenses;
        $this->start_count  = 0;
        $this->remains      = $remains;
        $this->saveOrFail();

        // 6) Отпарявляем в очеред на обновление статусов
        (new WiqStatusQueue($this->order_status, $this->id, $this->provider_id, $this->remains, $this->start_count))->send();

        return true;
    }

    function createFastOrder(Request $request): array
    {
        // 1) подгружаем данные по ключу продукта
        $product = Product::where('key', $request->key)->with('providerProduct.provider')->firstOrFail();

        // 2) Проверяем что сумма с фронта и с бека одинаковая
        $totalCost = round($product->cost * $request->amount, 3);
        $expenses  = round($product->providerProduct->cost * $request->amount, 3);
        if (isset($request->amountPosts)) {
            $totalCost = $totalCost * $request->amountPosts;
            $expenses  = $expenses * $request->amountPosts;
        }
        if ((float)$request->totalCost != $totalCost) {
            throw new \Exception('Total cost incorrect: ' . $totalCost . ' - ' . $request->totalCost);
        }

        // 3) Проверить есть ли пользователь?
        $isNewUser = false;
        $password  = '';
        $user      = User::where('email', $request->email)->first();
        if (empty($user->uuid)) {
            $isNewUser = true;
            // 3.1) Создаем нового пользователя
            $password       = Str::random(8);
            $user           = new User();
            $user->email    = strtolower($request->email);
            $user->password = $password;
            $user->balance  = 0;
            $user->saveOrFail();
            (new ActionRequest())->createConfirmEmailAction($user->email);
            Mail::to($user->email)->send(new SendNewAccountInfoMail($user->email, $password));
        }

        // 4) Генерируем ссылку на оплату
        $yookassa = new Yookassa();
        $payment  = $yookassa->createPayment($totalCost, $user->email);
        if (!isset($payment->id)) {
            throw new \Exception('$payment->id is not set');
        }
        $paymentLink = $payment->confirmation->getConfirmationUrl();

        // 5) Добавляем заказ
        $count = $request->amount;
        $remains = $count;
        if (isset($request->amountPosts)) {
            $remains = $request->amountPosts;
            $this->posts_count = $request->amountPosts;
        }

        $this->id           = mt_rand(100000000, 999999999);
        $this->user_uuid    = $user->uuid;
        $this->product_uuid = $product->uuid;
        $this->order_status = OrderStatuses::AWAITING_PAYMENT;
        $this->link         = $request->link;
        $this->count        = $count;
        $this->total_cost   = $totalCost;
        $this->expenses     = $expenses;
        $this->start_count  = 0;
        $this->remains      = $remains;
        $this->payment_link = $paymentLink;
        $this->saveOrFail();


        // 6) Добавляем запсись в испорию оплаты
        $paymentHistory                       = new PaymentHistory();
        $paymentHistory->user_uuid            = $user->uuid;
        $paymentHistory->payment_id           = $payment->id;
        $paymentHistory->payment_service_name = Yookassa::PAYMENT_SERVICE_NAME;
        $paymentHistory->amount               = $payment->amount->value;
        $paymentHistory->currency             = $payment->amount->currency;
        $paymentHistory->status               = $payment->status;
        $paymentHistory->callback             = json_encode([
            'action_name' => 'runOrderToWork',
            'params'      => [
                'order_uuid' => $this->uuid,
            ],
            'when'        => [
                'request_param' => 'event',
                'value'         => NotificationEventType::PAYMENT_SUCCEEDED,
            ],
        ]);
        $paymentHistory->saveOrFail();

        return [
            'paymentLink' => $paymentLink,
            'isNewUser'   => $isNewUser,
            'email'       => $user->email,
            'password'    => $isNewUser ? $password : null,
        ];
    }


    /**
     * Запуск услуги у провайдора
     *
     * @param $product
     * @param $quantity
     * @param $link
     * @param $quantityPosts
     *
     * @return CreateOrder
     * @var   $provider_class_name ServiceProviderInterface | WiqProvider
     *
     */
    private function creatingOrderWithProvider($product, $quantity, $link, $quantityPosts = null): CreateOrder
    {
        $providerClassName = $product->providerProduct->provider->class_name;
        $providerProductId = $product->providerProduct->provider_product_id;
        $providerClass     = 'App\Models\ProductProviders\\' . $providerClassName;
        $provider          = new $providerClass();

        return $provider->createOrder($providerProductId, $quantity, $link, $quantityPosts);
    }

    /**
     * Отменяем заказ
     * Просто меняем заказ в БД и возвращаем деньги
     *
     * @return bool
     */
    public function cancelOrder(): bool
    {
        $this->total_cost   = 0;
        $this->expenses     = 0;
        $this->order_status = OrderStatuses::STATUS_CANCELED;
        $this->save();

        return true;
    }


    /**
     * Отменяем заказ у провайдера, почле чего получаем сообщение
     * в очереди что заказ отменен и меняем статус в нашей БД
     *
     * @return bool
     */
    public function cancelOrderWithProvider(): bool
    {
        $providerClassName = $this->product->providerProduct->provider->class_name;

        $providerClass = 'App\Models\ProductProviders\\' . $providerClassName;
        $provider      = new $providerClass();

        return $provider->cancelOrder($this->provider_id);
    }

    /**
     * Отправка заказа на выполнение после получения оплаты
     *
     * @param Request $request
     *
     * @return bool
     * @throws \Throwable
     * @throws PayException
     */
    public function initOrderAfterGetPayment($orderUuid): bool
    {
        $order = Self::where('uuid', $orderUuid)->with('product.providerProduct.provider')->firstOrFail();

        // 1) Списываем деньги со счета
        User::where('uuid', $order->user_uuid)->firstOrFail()->pay($order->total_cost);

        // 2) Отправить на выполнение в wiq
        $order_id = $this->creatingOrderWithProvider(
            $order->product,
            $order->count,
            $order->link,
            $order->posts_count,
        )->provider_order_id;

        if (is_int($order_id)) {
            throw new \Exception('Something wrong! $order_id in incorrect. $order_id = ' . $order_id);
        }

        // 3) Изменить статус заказа
        $order->order_status = OrderStatuses::STATUS_PENDING;
        $order->provider_id  = $order_id;
        $order->saveOrFail();

        // 4) Отправить в сервис опрежения статистики
        (new WiqStatusQueue($order->order_status, $order->id, $order->provider_id, $order->remains, $order->start_count))->send();

        return true;
    }
}
