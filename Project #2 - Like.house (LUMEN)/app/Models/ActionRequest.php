<?php

namespace App\Models;


use App\Mail\ConfirmEmailMail;
use App\Mail\ResetPasswordMail;
use App\Traits\PrimaryKeyUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;


class ActionRequest extends Model
{
    use PrimaryKeyUuid;

    const CONFIRM_EMAIL_ACTION = 'email.confirm';
    const RESET_PASSWORD_ACTION = 'password.reset';

    protected $table = "action_request";

    protected $primaryKey = 'uuid';

    public $incrementing = false;

    /**
     * Добавляем запись о подтверждении eamil в БД и отпраяляем письмо с ссылкой
     *
     * @param $email
     *
     * @return bool
     * @throws \Exception
     */
    public function createConfirmEmailAction($email)
    {
        $this->hash   = $this->newHash();
        $this->action = self::CONFIRM_EMAIL_ACTION;
        $this->params = json_encode(['email' => $email]);

        if (!$this->save()) {
            throw new \Exception('Fail to save action request');
        }

        Mail::to($email)->send(new ConfirmEmailMail(['hash' => $this->hash]));

        return true;
    }

    /**
     * Добавляем запись о смене пароля в БД и отпраяляем письмо с ссылкой
     * @param $email
     *
     * @return bool
     * @throws \Exception
     */
    public function createResetPasswordAction($email)
    {
        $this->hash   = $this->newHash();
        $this->action = self::RESET_PASSWORD_ACTION;
        $this->params = json_encode(['email' => $email]);

        if (!$this->save()) {
            throw new \Exception('Fail to save action request');
        }

        Mail::to($email)->send(new ResetPasswordMail(['hash' => $this->hash]));

        return true;
    }

    public function newHash()
    {
        return sha1(uniqid());
    }

}
