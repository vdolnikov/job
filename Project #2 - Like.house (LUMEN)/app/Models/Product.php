<?php

namespace App\Models;

use App\Traits\PrimaryKeyUuid;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use PrimaryKeyUuid;

    protected $table = "products";

    public function providerProduct()
    {
        return $this->belongsTo(ProviderProduct::class, 'provider_product_uuid');
    }
}
