<?php

namespace App\Models;

use YooKassa\Client;
use YooKassa\Model\NotificationEventType;
use YooKassa\Model\Webhook\Webhook;

class Yookassa
{
    const PAYMENT_SERVICE_NAME = 'Yookassa';

    const STATUS_PENDING   = 'pending';
    const STATUS_WAITING   = 'waiting_for_capture';
    const STATUS_SUCCEEDED = 'succeeded';
    const STATUS_CANCELED  = 'canceled';

    private Client $client;

    /**
     * Yookassa constructor.
     */
    public function __construct()
    {
        $client       = new Client();
        $this->client = $client->setAuth(config('custom.yookassa_login') , config('custom.yookassa_password'));
    }

    /**
     * @return Client
     */
    private function getClient()
    {
        return $this->client;
    }

    /**
     * @param $amount
     * @param $userEmail
     *
     * @return \YooKassa\Request\Payments\CreatePaymentResponse|null
     * @throws \YooKassa\Common\Exceptions\ApiException
     * @throws \YooKassa\Common\Exceptions\BadApiRequestException
     * @throws \YooKassa\Common\Exceptions\ExtensionNotFoundException
     * @throws \YooKassa\Common\Exceptions\ForbiddenException
     * @throws \YooKassa\Common\Exceptions\InternalServerError
     * @throws \YooKassa\Common\Exceptions\NotFoundException
     * @throws \YooKassa\Common\Exceptions\ResponseProcessingException
     * @throws \YooKassa\Common\Exceptions\TooManyRequestsException
     * @throws \YooKassa\Common\Exceptions\UnauthorizedException
     */
    public function createPayment($amount, $userEmail)
    {
        return $this->getClient()->createPayment(
            [
                //Сумма платежа. Иногда партнеры ЮKassa берут с пользователя дополнительную комиссию, которая не входит в эту сумму.
                'amount'              => [
                    'value'    => (float)$amount,
                    'currency' => 'RUB',
                ],
                // Ссылка на страницу возврата
                'confirmation'        => [
                    'type'       => 'redirect',
                    'return_url' => config('custom.yookassa_return_link'),
                ],
                'receipt'      => [
                    'customer' => [
                        'email' => $userEmail,
                    ],
                ],
                'capture'             => true,
                'description'         => 'Пополнение баланса',
            ],
            uniqid('', true)
        );
    }

    /**
     * @param $paymentId
     *
     * @return \YooKassa\Model\PaymentInterface|\YooKassa\Request\Payments\PaymentResponse|null
     * @throws \YooKassa\Common\Exceptions\ApiException
     * @throws \YooKassa\Common\Exceptions\BadApiRequestException
     * @throws \YooKassa\Common\Exceptions\ExtensionNotFoundException
     * @throws \YooKassa\Common\Exceptions\ForbiddenException
     * @throws \YooKassa\Common\Exceptions\InternalServerError
     * @throws \YooKassa\Common\Exceptions\NotFoundException
     * @throws \YooKassa\Common\Exceptions\ResponseProcessingException
     * @throws \YooKassa\Common\Exceptions\TooManyRequestsException
     * @throws \YooKassa\Common\Exceptions\UnauthorizedException
     */
    public function getPaymentInfo($paymentId)
    {
        return $this->getClient()->getPaymentInfo($paymentId);
    }
}
