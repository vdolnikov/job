<?php

namespace App\Models;


use GuzzleHttp\Client;
use yii\base\Exception;

class Testmail
{
    const TOKEN = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvdGVzdG1haWwudG9wIiwiYXVkIjoiaHR0cHM6XC9cL2FwaS50ZXN0bWFpbC50b3AiLCJ1c2VyIjoiNjE5OGJhOWI5MzUyOCIsInN1YiI6ImFwaSIsImlhdCI6MTYzNzM5OTIwNiwiZXhwIjo0NzkzMDcyODA2fQ.1J5HYUgT6Kbkji25C1s2tU2PytbQf7NN_Ip1901RF7E';

    private $_client;

    /**
     * @return Client
     */
    private function getClient()
    {
        if (!isset($this->_client)) {
            $this->_client = new Client([
                'base_uri' => 'https://api.testmail.top/domain/check',
                'timeout'  => 3.0,
            ]);
        }

        return $this->_client;
    }


    /**
     * @param $email
     *
     * @return bool
     * @throws Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function checkEmail($email): bool
    {
        $params = [
            'data' => $email,
        ];

        $response = $this->send($params);

        if (!isset($response->result)) {
            throw new Exception('CheckEmail error! Response without result');
        }

        if ($response->result == false) {
            return false;
        }

        return true;
    }


    /**
     * Отпарвка запроса
     *
     * @param $params
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function send($params)
    {
        $response = $this->getClient()->request(
            'GET',
            '',
            [
                'query'   => $params,
                'headers' => [
                    'Authorization' => self::TOKEN,
                ],
            ],

        );

        if ($response->getStatusCode() !== 200) {
            throw new Exception('PROVIDER API (' . __CLASS__ . ') ERROR | PARAMS:' . json_encode($params));
        }

        return json_decode($response->getBody()->getContents());
    }
}
