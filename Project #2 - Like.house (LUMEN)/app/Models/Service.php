<?php

namespace App\Models;

use App\Traits\PrimaryKeyUuid;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use PrimaryKeyUuid;

    protected $table = "services";

    public $timestamps = false;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'deleted_at',
    ];

    public function serviceType()
    {
        return $this->hasMany(ServiceType::className());
    }
}
