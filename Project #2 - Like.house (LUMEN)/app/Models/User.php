<?php

namespace App\Models;

use App\Exceptions\PayException;
use App\Traits\PrimaryKeyUuid;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;


use Tymon\JWTAuth\Contracts\JWTSubject;
use Tymon\JWTAuth\Exceptions\PayloadException;

class User extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    use Authenticatable, Authorizable, SoftDeletes, PrimaryKeyUuid;

    protected $table = "users";

    protected $fillable = [
        'uuid',
        'email',
        'password',
        'balance',
        'email_verified_at',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'created_at',
        'updated_at',
        'deleted_at',
    ];


    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * @return mixed
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param $need
     *
     * @return bool
     */
    public function isBalanceEnough($need)
    {
        return $this->getBalance() >= $need;
    }

    /**
     * @param $cost
     *
     * @return bool
     * @throws PayException
     */
    public function pay($cost): bool
    {
        if(!$this->isBalanceEnough($cost)){
            throw new PayException("Недостаточно средств на счете. Пополните ваш баланс", 422);
        }

        $this->balance = $this->getBalance() - $cost;

        return $this->save();
    }
}
