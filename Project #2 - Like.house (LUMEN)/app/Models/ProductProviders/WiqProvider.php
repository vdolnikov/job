<?php

namespace App\Models\ProductProviders;


use App\Models\ProductProviders\ProviderMethods\CheckOrderStatus;
use App\Models\ProductProviders\ProviderMethods\CreateOrder;
use GuzzleHttp\Client;
use Mockery\Exception;

class WiqProvider extends Providers implements ServiceProviderInterface
{

    private $_client;

    private function getClient()
    {
        if (!isset($this->_client)) {
            $this->_client = new Client([
                'base_uri' => config('custom.wiq_api_link'),
                'timeout'  => 3.0,
            ]);
        }

        return $this->_client;
    }

    /**
     * Создание заказа
     *
     * @param $provider_service_id
     * @param $quantity
     * @param $link
     * @param $quantityPosts
     *
     * @return CreateOrder
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createOrder($provider_service_id, $quantity, $link, $quantityPosts = null)
    {

        $params = [
            'action'   => 'create',
            'service'  => $provider_service_id,
            'quantity' => $quantity,
            'link'     => $link,
        ];

        if (isset($quantityPosts)) {
            $params['posts'] = $quantityPosts;
        }

        $response = $this->send($params);

        if (!isset($response->order)) {
            throw new Exception('CreateOrder error! Response without order_id');
        }

        return new CreateOrder($response->order);
    }

    /**
     * Проверка статуса заказа
     *
     * @param $provider_order_id
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function checkStatus($provider_order_id)
    {
        $response = $this->send([
            'action' => 'status',
            'order'  => $provider_order_id,
        ]);

        if (!isset($response->status)) {
            throw new Exception('CheckStatus error! Response without status');
        }

        return new CheckOrderStatus($response->status, $response->start_count, $response->remains);
    }

    /**
     * Отмена заказа
     *
     * @param $provider_order_id
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function cancelOrder($provider_order_id): bool
    {
        $response = $this->send([
            'action' => 'cancel',
            'order'  => $provider_order_id,
        ]);

        return isset($response->cancel) && $response->cancel === "ok";
    }

    /**
     * Восстановление заказов, которые недовыполнены
     *
     * @param $provider_order_id
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function refillOrder($provider_order_id)
    {
        return $this->send([
            'action' => 'refill',
            'order'  => $provider_order_id,
        ]);
    }

    /**
     * Получение баланса
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getBalance()
    {
        return $this->send(['action' => 'balance']);
    }

    /**
     * Отпарвка запроса
     *
     * @param $params
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function send($params)
    {
        $allParams = array_merge(
            ['key' => config('custom.wiq_api_key')],
            $params
        );

        $response = $this->getClient()->request(
            'GET',
            '',
            ['query' => $allParams]
        );

        if ($response->getStatusCode() !== 200) {
            throw new Exception('PROVIDER API (' . __CLASS__ . ') ERROR | PARAMS:' . json_encode($params));
        }

        return json_decode($response->getBody()->getContents());
    }

}


