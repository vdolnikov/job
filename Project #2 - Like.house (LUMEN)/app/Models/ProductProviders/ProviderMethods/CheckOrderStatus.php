<?php

namespace App\Models\ProductProviders\ProviderMethods;


class CheckOrderStatus
{
    public int $remains;

    public string $status;

    public int $start_count;

    public function __construct($status, $start_count, $remains)
    {
        $this->status      = $status;
        $this->start_count = $start_count;
        $this->remains     = $remains;
    }
}
