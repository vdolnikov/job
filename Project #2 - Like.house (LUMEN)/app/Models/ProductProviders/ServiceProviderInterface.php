<?php

namespace App\Models\ProductProviders;


interface ServiceProviderInterface
{
    public function createOrder($provider_service_id, $quantity, $link);

    public function checkStatus($provider_order_id);

    public function cancelOrder($provider_order_id);

    public function refillOrder($provider_order_id);

    public function getBalance();

}


