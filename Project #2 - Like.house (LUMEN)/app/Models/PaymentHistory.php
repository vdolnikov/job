<?php

namespace App\Models;

use App\Traits\PrimaryKeyUuid;
use Illuminate\Database\Eloquent\Model;

class PaymentHistory extends Model
{
    use PrimaryKeyUuid;

    protected $table = "payment_history";

    /**
     * @param $params
     *
     * @return bool
     * @throws \App\Exceptions\PayException
     * @throws \Throwable
     */
    public function runOrderToWork($params){
        $order = new Order();
        return $order->initOrderAfterGetPayment($params->order_uuid);
    }

}
