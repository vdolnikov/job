<?php

namespace App\Models;

use App\Traits\PrimaryKeyUuid;
use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    use PrimaryKeyUuid;

    protected $table = "providers";

    public $timestamps = false;
}
