<?php

namespace App\Models;

use App\Traits\PrimaryKeyUuid;
use Illuminate\Database\Eloquent\Model;

class ServiceType extends Model
{
    use PrimaryKeyUuid;

    protected $table = "service_types";

    public $timestamps = false;

    public function Service()
    {
        return $this->belongsTo(Service::class, 'service_uuid');
    }
}
