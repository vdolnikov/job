<?php

namespace App\Models;

use App\Traits\PrimaryKeyUuid;
use Illuminate\Database\Eloquent\Model;

class ProviderProduct extends Model
{
    use PrimaryKeyUuid;


    protected $table = "provider_products";

    public function Product()
    {
        return $this->hasOne(Product::class, 'product_uuid');
    }

    public function Provider()
    {
        return $this->belongsTo(Provider::class, 'provider_uuid');
    }
}
