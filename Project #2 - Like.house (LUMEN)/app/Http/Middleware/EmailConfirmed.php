<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;

class EmailConfirmed
{

    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }


    public function handle($request, Closure $next)
    {

        if (is_null($this->auth->user()->email_verified_at)) {
            return response()->json(['email' => ["Ваш email не подтвержден!\nПисьмо с подтверждением отправлено на вашу почту"]], 401);
        }

        return $next($request);
    }
}
