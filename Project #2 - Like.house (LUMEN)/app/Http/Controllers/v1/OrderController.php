<?php

namespace App\Http\Controllers\v1;

use App\Exceptions\PayException;
use App\Models\Order;
use App\Models\OrderStatuses;
use App\Models\Testmail;
use App\Models\User;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PHPUnit\Util\Exception;
use stdClass;
use Tymon\JWTAuth\Facades\JWTAuth;


class OrderController extends Controller
{

    public function create(Request $request): JsonResponse
    {
        $this->validate($request, [
            'key'         => 'required|string|min:2|max:70',
            'amount'      => 'required|numeric|min:1|max:10000000',
            'amountPosts' => 'numeric|min:1|max:30',
            'totalCost'   => 'required|numeric|min:0|max:10000000',
            'link'        => 'required|string|min:2|max:500',
            'promo_code'  => 'string|min:2|max:500',
        ]);


        if (!str_starts_with($request->link, 'https://')) {
            $error       = new stdClass;
            $error->link = ["Неккоретная ссылка на профиль/пост"];

            return response()->json($error, 400);
        }

        try {
            $status = DB::transaction(function () use ($request) {
                $order = new Order();

                return $order->createOrder($request);
            });

            return response()->json(['success' => $status]);

        } catch (ModelNotFoundException $e) {
            Log::error($e->getMessage(), $e->getIds());

            return response()->json(['success' => false, 'error' => 'Ошибка при создании заказа'], 404);

        } catch (PayException $e) {

            return response()->json($e->getResponseMassage(), $e->getCode());

        } catch (\Exception | GuzzleException | \Throwable $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => ['Ошибка при создании заказа']], 500);
        }
    }

    public function getUserOrders(Request $request): JsonResponse
    {
        $authUser = JWTAuth::user();

        $orders = Order::where('user_uuid', $authUser->uuid)->with('product')->orderByDesc('created_at')->get();

        $orderList = [];
        foreach ($orders as $order) {
            $orderInfo              = new stdClass;
            $orderInfo->id          = $order->id;
            $orderInfo->productName = $order->product->name;
            $orderInfo->orderStatus = $order->order_status;
            $orderInfo->link        = $order->link;
            $orderInfo->count       = $order->count;
            $orderInfo->postsCount  = $order->posts_count;
            $orderInfo->totalCost   = $order->total_cost;
            $orderInfo->startCount  = $order->start_count;
            $orderInfo->remains     = $order->remains;
            $orderInfo->paymentLink = $order->payment_link;
            $orderInfo->createdAt   = $order->created_at;

            $orderList[] = $orderInfo;
        }

        return response()->json($orderList);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function changeOrderStatus(Request $request): JsonResponse
    {
        $this->validate($request, [
            'id'          => 'required|numeric',
            'status'      => 'required|string',
            'remains'     => 'string',
            'start_count' => 'string',
        ]);

        $status = DB::transaction(function () use ($request) {
            $order = Order::where('id', $request->id)->firstOrFail();

            if ($order->order_status == OrderStatuses::STATUS_ERROR) {
                return true;
            }

            // Проверка для того чтобы не могли несколько раз вернуть деньги по заказам, которые в статусе "Ошибка"
            if ($request->status == OrderStatuses::STATUS_ERROR || $request->status == OrderStatuses::STATUS_CANCELED) {
                $user          = User::where('uuid', $order->user_uuid)->firstOrFail();
                $user->balance = $user->balance + $order->total_cost;
                $user->save();

                $order->total_cost = 0;
                $order->expenses   = 0;
            }

            // В случае прерывания, когда уже несколько добавлено
            // Если $order->posts_count не null то значит заказ был на авто лайки/просмотры, в таком случае мы не возвращаем деньги
            if ($request->status == OrderStatuses::STATUS_PARTIAL && $order->posts_count == null) {

                $total_cost = (($order->count - $request->remains) * $order->total_cost) / $order->count;
                $expenses   = (($order->count - $request->remains) * $order->expenses) / $order->count;

                $user          = User::where('uuid', $order->user_uuid)->firstOrFail();
                $user->balance = ($user->balance + $order->total_cost) - $total_cost;
                $user->save();

                $order->total_cost = $total_cost;
                $order->expenses   = $expenses;
            }


            if (in_array($request->status, OrderStatuses::getFinalStatuses())) {
                $request->remains = 0;
            }

            $order->remains      = (int)$request->remains;
            $order->start_count  = (int)$request->start_count;
            $order->order_status = $request->status;

            return $order->save();
        });

        return response()->json(['success' => $status]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function cancelOrder(Request $request): JsonResponse
    {
        $this->validate($request, [
            'id' => 'required|numeric',
        ]);

        try {
            $status = DB::transaction(function () use ($request) {
                $authUser = JWTAuth::user();
                $order    = Order::where('id', $request->id)->with('product.providerProduct.provider')->firstOrFail();

                if ($authUser->uuid !== $order->user_uuid) {
                    throw new Exception('Cant stop order, because user is not owner of this order');
                }

                if ($order->order_status === OrderStatuses::AWAITING_PAYMENT) {
                    $res = $order->cancelOrder();
                } else {
                    $res = $order->cancelOrderWithProvider();
                }

                return $res;
            });


            return response()->json(['success' => $status]);

        } catch (\Exception | GuzzleException | \Throwable $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => ['Fail to stop order']], 500);
        }

    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function createFastOrder(Request $request): JsonResponse
    {
        $this->validate($request, [
            'email'       => 'required|email|max:100',
            'key'         => 'required|string|min:2|max:70',
            'amount'      => 'required|numeric|min:1|max:10000000',
            'amountPosts' => 'numeric|min:1|max:30',
            'totalCost'   => 'required|numeric|min:0|max:10000000',
            'link'        => 'required|string|min:2|max:500',
            'promo_code'  => 'string|min:2|max:500',
        ]);


        if (!str_starts_with($request->link, 'https://')) {
            $error       = new stdClass;
            $error->link = ["Неккоретная ссылка на профиль/пост"];

            return response()->json($error, 400);
        }

        // Проверяем email на валидность
        $testEmail = new Testmail();
        if(!$testEmail->checkEmail(strtolower($request->email))){
            return response()->json(['email' => ['Email не поддерживается']], 429);
        }

        try {
            $status = DB::transaction(function () use ($request) {
                $order = new Order();

                return $order->createFastOrder($request);
            });

            return response()->json($status);

        } catch (ModelNotFoundException $e) {
            Log::error($e->getMessage(), $e->getIds());

            return response()->json(['success' => false, 'error' => 'Ошибка при создании заказа'], 404);

        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => ['Ошибка при создании заказа']], 500);
        }
    }


}
