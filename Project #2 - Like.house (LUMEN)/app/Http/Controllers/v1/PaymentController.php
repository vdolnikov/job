<?php

namespace App\Http\Controllers\v1;

use App\Models\PaymentHistory;
use App\Models\User;
use App\Models\Yookassa;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PHPUnit\Util\Exception;
use Tymon\JWTAuth\Facades\JWTAuth;
use YooKassa\Model\NotificationEventType;


class PaymentController extends Controller
{

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request): JsonResponse
    {
        $this->validate($request, [
            'amount' => 'required|numeric|min:1|max:1000000',
        ]);

        try {
            $authUser = JWTAuth::user();

            $yookassa = new Yookassa();
            $payment  = $yookassa->createPayment($request->amount, $authUser->email);
            if (!isset($payment->id)) {
                throw new Exception('$payment->id is not set');
            }

            $paymentHistory                       = new PaymentHistory();
            $paymentHistory->user_uuid            = $authUser->uuid;
            $paymentHistory->payment_id           = $payment->id;
            $paymentHistory->payment_service_name = Yookassa::PAYMENT_SERVICE_NAME;
            $paymentHistory->amount               = $payment->amount->value;
            $paymentHistory->currency             = $payment->amount->currency;
            $paymentHistory->status               = $payment->status;
            $paymentHistory->saveOrFail();

            $response = [
                'confirmation_url' => $payment->confirmation->getConfirmationUrl(),
            ];

            return response()->json($response);

        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Fail to get payment link'], 500);
        }
    }

    /**
     * @param Request $request
     *
     * @var $actionName PaymentHistory //Методы из этого класса
     * @return JsonResponse
     */
    public function setPaymentStatus(Request $request)
    {
        try {
            $status = DB::transaction(function () use ($request) {
                $paymentHistory         = PaymentHistory::where('payment_id', $request->object['id'])->firstOrFail();
                if($paymentHistory->status == Yookassa::STATUS_SUCCEEDED){
                    return true;
                }

                $paymentHistory->status = $request->object['status'];
                $paymentHistory->saveOrFail();

                if ($request->event === NotificationEventType::PAYMENT_SUCCEEDED) {
                    $user          = User::where('uuid', $paymentHistory->user_uuid)->firstOrFail();
                    $user->balance = $user->balance + $paymentHistory->amount;

                    // Этот блок нужен для пользователей кто оплатил, но не подтвердили почту
                    // Автоматически просмавляем флаг что email подтвержден
                    // Для того чтобы не усложнять жизнь клиенту, который принес деньги
                    if(is_null($user->email_verified_at)){
                        $user->email_verified_at = date('Y-m-d H:i:s');
                    }

                    if($paymentHistory->amount >= 500){
                        $user->balance = $user->balance + 100;
                    }

                    $user->saveOrFail();
                }

                // Вызов callback функций из payment_history
                // Если там есть какое то действие, то оно отправляется на выполнение
                // Можно указывать на какой параметр, с каким значением реакировать и какой метод вызывать
                if(!is_null($paymentHistory->callback)){
                    $callback = json_decode($paymentHistory->callback);
                    $actionName = $callback->action_name;
                    $requestParams = $callback->when->request_param;

                    if($request->$requestParams === $callback->when->value){
                        $paymentHistory->$actionName($callback->params);
                    }
                }

                return true;
            });

            if (!$status) {
                return response()->json(['success' => false], 400);
            }

            return response()->json(['success' => true]);

        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Fail to change payment status'], 500);
        }
    }


}
