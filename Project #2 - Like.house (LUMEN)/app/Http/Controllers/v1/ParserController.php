<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;



class ParserController extends Controller
{

    public function instagramSubscribers (Request $request)
    {
        $this->validate($request, [
            'url' => 'required|string'
        ]);

//        https://rapidapi.com/yuananf/api/instagram28/
//        $curl = curl_init();
//        curl_setopt_array($curl, [
//            CURLOPT_URL => "https://instagram28.p.rapidapi.com/user_info?user_name=".$request->url,
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_FOLLOWLOCATION => true,
//            CURLOPT_ENCODING => "",
//            CURLOPT_MAXREDIRS => 10,
//            CURLOPT_TIMEOUT => 30,
//            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//            CURLOPT_CUSTOMREQUEST => "GET",
////            CURLOPT_HTTPHEADER => [
////                "x-rapidapi-host: instagram28.p.rapidapi.com",
////                "x-rapidapi-key: 07f45b51abmsh40b2ea9a0119e76p1dbf5ejsna12f03aa8e59"
////            ],
//            CURLOPT_HTTPHEADER => [
//                "x-rapidapi-host: instagram28.p.rapidapi.com",
//                "x-rapidapi-key: a01fb0b5e0msh9636a22bec0ddbcp1b18dcjsnc49c6ba953e2"
//            ],
//        ]);
//
//        $response = json_decode(curl_exec($curl));
//        $err = curl_error($curl);
//        curl_close($curl);
//
//        if ($err) {
//            return response()->json(['success' => false, 'error' => 'Not found'], 404);
//        }

        return response()->json([
//            'success' => true,
//            'link' => "https://www.instagram.com/".$request->url,
//            'logo' => $response->data->user->profile_pic_url,
//            'followers' => $response->data->user->edge_followed_by->count

                'success' => true,
                'link' => "https://www.instagram.com/".$request->url,
                'logo' => "https://scontent-iad3-1.cdninstagram.com/v/t51.2885-19/s150x150/130196801_2744736422521699_1280564321903376475_n.jpg?_nc_ht=scontent-iad3-1.cdninstagram.com&_nc_ohc=_PD8WbQ9tQgAX_VAR-t&edm=ABfd0MgBAAAA&ccb=7-4&oh=6f555d352224308a75cb8bf26b67e3b8&oe=615C5834&_nc_sid=7bff83",
                'followers' => 512
        ]);
    }

}
