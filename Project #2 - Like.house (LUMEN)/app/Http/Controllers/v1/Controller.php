<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller as CommonController;
use GuzzleHttp\Client;


class Controller extends CommonController
{

    /**
     * Проверка google капчи по токену
     *
     * @param $token
     *
     * @return bool
     */
    protected function checkGoogleRecaptchaToken($token)
    {
        if ($token !== config('custom.google_captcha_unittest_secret')) {
            $client   = new Client();
            $response = $client->get('https://www.google.com/recaptcha/api/siteverify', [
                'query' => [
                    'secret'   => config('custom.google_captcha_secret'),
                    'response' => $token,
                ],
            ]);

            $response = json_decode($response->getBody()->getContents());

            return $response->success;
        }

        return true;
    }
}
