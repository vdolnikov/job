<?php

namespace App\Http\Controllers\v1;

use App\Models\ActionRequest;
use App\Models\Testmail;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Получить список пользователей
     * Или конкретного пользователя по uuid, email, url_name
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getUserList(Request $request)
    {
        $this->validate($request, [
            'email'    => 'email|max:100',
            'uuid'     => 'uuid'
        ]);

        try {
            if (isset($request->uuid)) {
                $users = User::where('uuid', $request->uuid)->firstOrFail();
            } elseif (isset($request->email)) {
                $users = User::where('email', $request->email)->firstOrFail();
            } else {
                $users = User::simplePaginate(20);
            }

            return response()->json($users);
        } catch (ModelNotFoundException $e) {
            Log::error($e->getMessage(), $e->getIds());

            return response()->json(['success' => false, 'error' => 'User not found'], 404);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Fail to get user info'], 500);
        }
    }

    /**
     * Получить информацию о пользователе по UUID
     *
     * @param $uuid
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function getUserInfoByUUID($uuid, Request $request)
    {
        $request['uuid'] = $uuid;
        $this->validate($request, [
            'uuid' => 'uuid',
        ]);

        try {
            $userInfo = User::where('uuid', $uuid)->firstOrFail();

            return response()->json($userInfo);
        } catch (ModelNotFoundException $e) {
            Log::error($e->getMessage(), $e->getIds());

            return response()->json(['success' => false, 'error' => 'User not found'], 404);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Fail to get user info'], 500);
        }
    }

    /**
     * Добавление нового пользователя
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Throwable
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'name'                  => 'string|nullable|max:100',
            'email'                 => 'required|email|unique:users|max:100',
            'password'              => 'required|min:5|max:100',
            'password_confirmation' => 'required|same:password',
            'token'                 => 'required|string',
        ]);

        if (!$this->checkGoogleRecaptchaToken($request->token)) {
            return response()->json(['message' => 'You are robot'], 429);
        }

        try {
            $email = strtolower($request->email);

            // Проверяем email на валидность
            $testEmail = new Testmail();
            if(!$testEmail->checkEmail($email)){
                return response()->json(['email' => ['Email не поддерживается']], 429);
            }

            $user           = new User();
            $user->email    = $email;
            $user->password = $request->password;
            $user->balance  = '0';
            $user->saveOrFail();

            // Отправляем письмо подтверждения email и создание события на подтверждение емейла
            (new ActionRequest())->createConfirmEmailAction($user->email);

        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => ['Fail to add new user']], 500);
        }

        return response()->json($user, 201);
    }

    /**
     * Обновление пололя пользователя
     *
     * @param         $uuid
     * @param Request $request
     */
    public function updatePasswordByUUID($uuid, Request $request)
    {
        $request['uuid'] = $uuid;

        $rules = [
            'uuid'                  => 'uuid',
            'password'              => 'required|min:5|different:password_old',
            'password_confirmation' => 'required|same:password',
            'password_old'          => 'required',
        ];

        try {
            $user = User::findOrFail($uuid);

            if (!$request->user()->can('change-self-data', $user)) {
                return response()->json(['success' => false, 'error' => 'Access denied for user'], 403);
            }

            $validator = Validator::make($request->all(), $rules);

            $validator->after(function ($validator) use ($user, $request) {
                if (!Hash::check($request->password_old, $user->password)) {
                    $validator->errors()->add('password_old', 'Old password does not match');
                }
            });

            if ($validator->fails()) {
                return response($validator->errors(), 422);
            }

            $status = $user->update(['password' => $request->password]);
            if (!$status) {
                throw new \Exception('Fail to update user');
            };

            return response()->json(['success' => true]);

        } catch (ModelNotFoundException $e) {
            Log::error($e->getMessage(), $e->getIds());

            return response()->json(['success' => false, 'error' => 'User not found'], 404);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Fail to update user'], 500);
        }
    }
}
