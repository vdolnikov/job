<?php

namespace App\Http\Controllers\v1;

use App\Models\Product;
use Illuminate\Http\Request;



class ProductController extends Controller
{

    public function getProductInfo (Request $request)
    {
        $this->validate($request, [
            'key' => 'required|string'
        ]);

        $product = Product::where('key', $request->key)->first();


        return response()->json($product);
    }

}
