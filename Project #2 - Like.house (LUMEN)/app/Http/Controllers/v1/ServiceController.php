<?php

namespace App\Http\Controllers\v1;

use App\Models\Product;
use App\Models\Service;
use App\Models\ServiceType;
use App\Models\SocialNetwork;
use Illuminate\Http\Request;


class ServiceController extends Controller
{

    public function getServices()
    {
        $socialNetworks = SocialNetwork::whereNull('deleted_at')->orderBy('position')->get();
        $services = Service::whereNull('deleted_at')->orderBy('position')->get();
        $servicesTypes = ServiceType::orderBy('position')->get();


        foreach ($socialNetworks as $socialNetwork){
            $newService = [];
            foreach ($services as $service) {
                if($socialNetwork['uuid'] == $service['social_network_uuid']){
                    $newServiceTypes = [];
                    foreach ($servicesTypes as $servicesType) {
                        if($servicesType->service_uuid == $service->uuid){
                            unset($servicesType->uuid, $servicesType->service_uuid);
                            $newServiceTypes[] = $servicesType;
                        }
                    }
                    unset($service->uuid, $service->social_network_uuid);
                    $service['serviceTypes'] = $newServiceTypes;
                    $newService[] = $service;
                }
            }
            unset($socialNetwork->uuid);
            $socialNetwork['services'] = $newService;

        }

        return response()->json($socialNetworks);
    }

}
