<?php

namespace App\Http\Controllers\v1;

use App\Models\ActionRequest;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Facades\JWTAuth;


class EmailController extends Controller
{
    /**
     * Отправлка ссылки на сброс пароля
     *
     * @param Request $request
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function sendMailToResetPassword(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'token' => 'required|string',
        ]);

        if (!$this->checkGoogleRecaptchaToken($request->token)) {
            return response()->json(['message' => 'You are robot'], 429);
        }

        try {
            $user = User::where('email', $request->email)->firstOrFail();
            (new ActionRequest())->createResetPasswordAction($user->email);
        } catch (ModelNotFoundException $e) {
            Log::error($e->getMessage(), $e->getIds());

            return response()->json(['success' => false, 'error' => 'User not found'], 404);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Fail to send reset password email'], 500);
        }

        return response()->json(['success' => true], 200);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function resendMailToVerificationEmail(Request $request)
    {
        try {

            $authUser = JWTAuth::user();

            if (is_null($authUser->email_verified_at)) {
                // Отправляем письмо подтверждения email и создание события на подтверждение емейла
                (new ActionRequest())->createConfirmEmailAction($authUser->email);
            }

            return response()->json(['success' => true], 200);

        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json(['success' => false, 'error' => 'Fail to resend verification email'], 500);
        }

    }

}
