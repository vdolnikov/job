<?php

namespace App\Traits;

use Illuminate\Support\Str;

trait PrimaryKeyUuid
{
    /**
     * @return string
     */
    public function getKeyName()
    {
        return 'uuid';
    }

    /**
     * @return string
     */
    public function getKeyType()
    {
        return 'string';
    }

    /**
     * @return bool
     */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * Генерирует UUID для модели
     */
    protected static function bootPrimaryKeyUuid()
    {
        static::creating(function ($model) {
            if (!$model->getKey()) {
                $model->{$model->getKeyName()} = Str::uuid()->toString();
            }
        });
    }
}
