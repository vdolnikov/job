<?php

namespace App\Queue;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class WiqStatusQueue
{
    private $date;

    public function __construct($status, $id, $provider_id, $remains, $start_count)
    {
        $data              = new \stdClass();
        $data->status      = (string)$status;
        $data->id          = (string)$id;
        $data->product_id  = (string)$provider_id;
        $data->remains     = (string)$remains;
        $data->start_count = (string)$start_count;
        $this->date        = $data;

        return $this;
    }

    public function send()
    {
        $connection = new AMQPStreamConnection(config('custom.rabbit_host'), config('custom.rabbit_port'), config('custom.rabbit_user'), config('custom.rabbit_pass'));
        $channel    = $connection->channel();
        $channel->queue_declare('queue.normal', false, true, false, false);

        $msg = new AMQPMessage(json_encode($this->date));
        $channel->basic_publish($msg, 'exchange.normal', 'normalKey');

        $channel->close();
        $connection->close();
    }

}
