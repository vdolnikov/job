<?php

namespace App\Providers;

use App\Models\Permission;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\ServiceProvider;


class PermissionServiceProvider extends ServiceProvider
{
    /**
     * Register any auth services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any auth services.
     *
     * @return void
     */
    public function boot()
    {
        try {
            Gate::define('change-self-data', function ($user, $data) {
                return $user->uuid === $data->uuid;
            });

            Gate::define('mail-confirmed', function ($user, $data) {
                return (boolean)$user->email_verified_at;
            });

        } catch (\Exception $e) {
            report($e);

            return false;
        }
    }
}
