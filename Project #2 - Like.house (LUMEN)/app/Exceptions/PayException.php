<?php

namespace App\Exceptions;

use JetBrains\PhpStorm\Pure;
use stdClass;

class PayException extends \Exception
{
    /**
     * @return stdClass
     */
    #[Pure] public function getResponseMassage(): stdClass
    {
        $error      = new stdClass;
        $error->pay = [$this->getMessage()];

        return $error;
    }
}
